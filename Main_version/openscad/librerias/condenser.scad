use <./illumination.scad>;
include <./microscope_parameters.scad>;
include <./Light_Arm.scad>;
include <./Microbrew manipulator_v0.scad>;


////// Brazo iluminación + luz y condenser openflexure//////

//acople_condenser();
module acople_condenser(){
translate([18,-(tabWidth+gapBuffer+tol)-2,-10+2]) rotate([0,90,0])
cube([tabWidth,tabWidth,Ancho_acople*4],center=true);
difference(){
translate([0,0,-10])
cylinder(h=4,r=12);
translate([0,0,-10])

cylinder(h=10,r=6);
}
tall_condenser();}

wall = 3;
X_range = 44; // Length(70) - B_X(20) - 2*wall(6) = 44
A_X = X_range+20+2*wall;
A_extra_cam_height=12;
Ancho_acople=11;//acople de la luz a la base

////// Brazo iluminación Articulado///////////



base_light();
//base_light_art();
base_light_fix();

tol=1.7;
tol1=1;
acople_base_fix();
module acople_base_fix(){
difference(){
union(){
translate([0,-(tabWidth+gapBuffer+tol1),0]) 
#cube([tabLength+gapBuffer+tol1,tabWidth+gapBuffer+tol,Ancho_acople],center=true);//holder condensador
    translate([0,-(tabWidth+gapBuffer+tol1)*2+1,0]) 
cube([tabLength+gapBuffer+tol,tabWidth/2+gapBuffer+tol,Ancho_acople],center=true);
    }
translate([0,-(tabWidth+gapBuffer+tol)+1,+1]) rotate([0,90,0])
#cube([tabWidth+tol1,tabWidth+tol1,Ancho_acople*2],center=true);
translate([0,-(tabWidth+gapBuffer)-(tabWidth+gapBuffer)-tol1/2-1/2,0])
nuttrap1();
   translate([0,-tabWidth-gapBuffer-tol,1]) #rotate([90,0,0]) cylinder(h=20,r=1.7);
}

//Set Screw Diameter
Set_Screw_Diameter1=2.9; //[2:10]
//Set Screw Nut Diameter
Set_Screw_Nut_Diameter1=5.5; //[4:20]
//Set Screw Nut Thickness
Set_Screw_Nut_Thickness1=2; //[2:10]

a1=Set_Screw_Diameter1; //[2:10]
//Set Screw Nut Diameter
b1=Set_Screw_Nut_Diameter1; //[4:20]
//Set Screw Nut Thickness
c1=Set_Screw_Nut_Thickness1; //[2:10]

module nuttrap1(){
translate([0,(c1+1)/2,0])rotate([90,0,0])hexagon(c1+1,(b1+1)/2);
translate([0,0,(b1*3)/2])cube([b1+1,c1+1,b1*3],center = true);
}

difference(){
union(){
cube([tabLength+gapBuffer+tol,tabWidth+gapBuffer+tol,Ancho_acople],center=true);
translate([0,tabWidth+gapBuffer+tol-1,1/2])
cube([tabLength+gapBuffer+tol,tabWidth*2,Ancho_acople+1],center=true);
}

cube([tabLength+tol,tabWidth+tol,Ancho_acople*2],center=true);
translate([0,tabWidth+gapBuffer+tol-1,1/2])
nuttrap();
    rotate([-90,0,0]) translate([0,-1,-2])
  cylinder(h=20,r=2.5);
}   
   
}
module base_light_fix(){
arm_length=90;
translate([tabLength/4,(A_Y+A_extrawall*2)+A_extra_cam_sep+3*wall+3.5*wall+0.8,-wall-1/2])rotate([0,0,90]) 
		cube ([tabLength,tabWidth,arm_length ]); 

l=A_X/4;
w=Ancho_acople;
h=25;
rotate([0,0,90]) translate([(A_Y+A_extrawall*2)+A_extra_cam_sep+3*wall+2.5*wall,-l,-wall-1/2]) prism(w,l,h);

rotate([0,0,-90]) translate([-((A_Y+A_extrawall*2)+A_extra_cam_sep+3*wall+2.5*wall)-w,-l,-wall-1/2]) prism(w,l,h);
}
module prism(l, w, h){
       polyhedron(
               points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]],
               faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
               );}
   
module base_light_art(){;
translate([tabLength/4,(A_Y+A_extrawall*2)+A_extra_cam_sep+3*wall+3.5*wall+0.8,-wall-1/2])rotate([0,0,90]) difference(){
		cube ([tabLength,tabWidth, holeSize+holeBuffer*2]); 
    translate([tabLength/2,tabWidth*2,(holeSize+holeBuffer*2)/2])
            rotate([90,0,0]) cylinder (tabWidth+gapBuffer*2+2, holeSize/2, holeSize/2, $fn=30);//acople brazo
        }}

module base_light(){
difference(){
    union(){
translate([-A_X/4,(A_Y+A_extrawall*2)+A_extra_cam_sep+3*wall+2.5*wall,-A_Z_lower-A_Z/2+wall/2])cube([A_X/2,Ancho_acople,A_extra_cam_height]); //cubo base

translate([-A_X/4,(A_Y+A_extrawall*2)+A_extra_cam_sep+4*wall+2.5*wall,-A_Z_lower-A_Z/2])
               cube([A_X/2,Ancho_acople-wall,wall]); //escalon

        }
    
translate([A_X/4-3*wall,(A_Y+A_extrawall*2)+A_extra_cam_sep+4*wall+A_extrawall+2.5*wall/2+Ancho_acople/2,-A_Z_lower-A_Z/2+5*wall/2])rotate([0,90,0]) nuttrap();
        translate([-(A_X/4-3*wall),(A_Y+A_extrawall*2)+A_extra_cam_sep+4*wall+A_extrawall+2.5*wall/2+Ancho_acople/2,-A_Z_lower-A_Z/2+5*wall/2])rotate([0,-90,0])nuttrap();
        #translate([-(A_X/4-3*wall)-2.5,(A_Y+A_extrawall*2)+A_extra_cam_sep+4*wall+10+Ancho_acople,-A_Z_lower-A_Z/2+4*wall/2+wall/2-2.5])rotate([90,0,0])cube([5,5,30]);
                #translate([-(A_X/4-3*wall)-2.5,(A_Y+A_extrawall*2)+A_extra_cam_sep+4*wall+10+Ancho_acople,-A_Z_lower-A_Z/2+4*wall/2+wall/2])rotate([90,0,0])cylinder(h=30,r=2.5);
        #translate([-(A_X/4-3*wall)+2.5,(A_Y+A_extrawall*2)+A_extra_cam_sep+4*wall+10+Ancho_acople,-A_Z_lower-A_Z/2+4*wall/2+wall/2])rotate([90,0,0])cylinder(h=30,r=2.5);
    #translate([A_X/4-3*wall-2.5,(A_Y+A_extrawall*2)+A_extra_cam_sep+4*wall+10+Ancho_acople,-A_Z_lower-A_Z/2+4*wall/2+wall/2-2.5])rotate([90,0,0]) cube([5,5,30]);
        #translate([A_X/4-3*wall+2.5,(A_Y+A_extrawall*2)+A_extra_cam_sep+4*wall+10+Ancho_acople,-A_Z_lower-A_Z/2+4*wall/2+wall/2])rotate([90,0,0])cylinder(h=30,r=2.5);
              #translate([A_X/4-3*wall-2.5,(A_Y+A_extrawall*2)+A_extra_cam_sep+4*wall+10+Ancho_acople,-A_Z_lower-A_Z/2+4*wall/2+wall/2])rotate([90,0,0])cylinder(h=30,r=2.5);

translate([-A_X/4,(A_Y+A_extrawall*2)+A_extra_cam_sep+3*wall+2.5*wall,-A_Z_lower-A_Z/2])
               cube([A_X/2,wall,wall]);  
    
}}

