/*This project is based on:
1) optical designs made for Comunicaciones Especulativas: http://interspecifics.cc/comunicacionesespeculativas/ and https://osf.io/c542q/ - which recreates -as a 3D printed version- the optical commercial pieces used by Alexandre Kabla and his team for the openlabtools microscope (http://openlabtools.eng.cam.ac.uk/Instruments/Microscope/Optics/)

2) <picam_2_push_fit.scad> from OpenFlexure Microscope: Raspberry Pi Camera v2 push-fit mount (c) Richard Bowman, January 2016 Released under the CERN Open Hardware License  

3) https://github.com/adrianschlatter/threadlib   (before I used <threads.scad> from http://dkprojects.net/openscad-threads/ but it takes ages and some computers would never render designs...)

All this code is licensed under the Creative Commons - Attribution  (CC BY 3.0)
Fernán Federici and Nano Castro 2020
*/

//notes, EO tubes are C-Mount 25,4 32TPI and inside light path is M22x0.5 for LIGHT BAFFLE THREAD


use <utilities.scad>
use <logitech_c270.scad>
include <picam_2_push_fit.scad>         
use <./librerias/illumination.scad>
use  <./librerias/BM2L_v2.scad>
//include <./librerias/logitech_c270_FF.scad>; 

////---- new thread library -----------
use <./threadlib/threadlib.scad>


corr=0.2;// used for printing imperfections when one piece has to go into another; highly reccommended
wall=2.5;
M12_r=12/2;
mount_h = 10;
int_r=22/2;// internal r of cylinder according to edmund optics
//RMS_r=(0.8*25.4)/2; //10,16 mm of r for DIN (and JIS) objectives that uses RMS, this is about 20.3mm (with thread step of about 0.7mm).
tube_r=29.85/2;//external size
c_mount_h=4;
M27_r=27/2; //M27 threads
ext_bot_len=11.3+10+5;
foc_adj_r=21.5/2;//width of thread for focus control
int_focal_corr_h=16;//total length to adjust focus without crashing to lens of top bottom piece
lens_holder_pos=8.5; //distance of lens holder notch from bottom end of top part 
filter_cube_x=40;
filter_cube_y=40;
filter_cube_z=35;//24;
x_triangle=19;//to have hipotenuse of >25
y_triangle=19;//to have hipotenuse of >25

x_triangle_ver=25.5;//to have hipotenuse of >36 for a vertical orientation of dichroic
y_triangle_ver=25.5;//to have hipotenuse of >36 for a vertical orientation of dichroic
// sqr 36=  sqr x_triangle_ver + sqr  y_triangle_ver
M3_r=3/2;
M3_h=10;
joint_x=5;
joint_y=5;
joint_z=2;
extra_doublet=10; 
base_h=6;
mounting_hole_x = 8.25;
thread_h_cam_attach=10;//height of thread holding the RPI adapter
$fn=60;
thread_thick=2; //add thickness for external thread used in focus adjustment
notch_x=thread_thick*2;//notch for avoiding rotation
notch_y=2;//notch for avoiding rotation
htt_h=45;//height of notches in holder_of_threaded_tube module
notch_ring_h=25;    
rodamiento_i_r=40/2;
rodamiento_e_r=68/2;
rodamiento_h=15;//https://www.bearingkingdom.com/ball-bearings/deep-groove-radial-ball-bearings/6008_ZZ_C3_KOYO_Deep_Groove_Bearing___40x68x15mm.html
dedos_d=18; 
ruedita_r=tube_r+thread_thick*14;             
cono_h=50; //altura cono     
slide_y=75;
slide_x=26;
slide_z=1;      
push_joint_h=20; //union entre cono y base de iluminacion    
push_joint_xy=40;        
cone_slide_h=cono_h+20;//mas alto para meter 100X  
slide_holder_z=slide_z*23;
thick=3;
ring_h=2.5;
push_joint_xy_V2=push_joint_xy*1.7;
ocular_r=23.2/2;
ocular_h=int_focal_corr_h+7;
     
insert_h=20; // esto dice cuanto de largo le doy al tubo que luego entra el ext_tube part
ocular_cam_h=4;
ocular_in_h=22; //distancia in del ocular amazon 18mm
M12ext_h=20;
M12_ring_r=14/2;
M12_ring_h=2;//altura de borde M12
internal_space_h=M12ext_h+insert_h/4-wall*3-M12_ring_h;
ring_M12_h=8;
base_h_led_holder=base_h*3;     
enganche=6;//thereaded part that attaches the camera holder to the opetical tube
excess_obj=24; 
dic_w=37;       
type = "M33x0.75";//"M27x0.75"; //main thread type of the optical tube and knob
//type_test="M33x0.75-ext";//"G1/2-ext";
vueltas=37;
tube_lens_totalh=ext_bot_len+extra_doublet*2+int_focal_corr_h; 
echo ("tubelens height= ", tube_lens_totalh);
internal_path_r= 11; //22mm is EO internal path //to try later a wider internal path 
ocular_cam_h=4;
ocular_in_h=22; //distancia in del ocular amazon 18mm      
ring_inf_h=5;
leg_xy=12;  
leg_h=50;
ext_4_5_h=15;
ext_40_h=7;
rosca_h=4.5;
 

//The order of modules - from top to bottom - is:

//--------------------------------1) ocular /camera adapters 
       
//c270_holder();//for logitech c270 camerra
//ocular_cam_adapter();
//        topcase_CE_c270(); //to cover the back of the logitech cam

//---------------------------------2) superior ring

//knob_ring_sup();//with two holes

//------------------------3) main optical tube

//tube_lens_threaded_ocular();

//--------------------------4) M12 holder

//ring_M12();   
//ext_M12tube();

//----------------------------5) extension tube to hold objective

//    ext_tube_4_5X();
//    ext_tube_40X();  
    
//-------------------------6) inferior ring

//ring_inf();  //top ring to hold springs

//----------------------------------------------generic modules used to build others
    
//ringOPTO(); //emulate the ring of OPTO LEDS
    
module ringOPTO(){
 difference(){
     cylinder(r=69.85/2, h=2); 
    cylinder(r=40/2, h=2);
 }
}

//pos_cylinder_thread(20,"M33x0.75-ext");//H define the height in mm and type of thread
module pos_cylinder_thread(H, type_of_thread){
    specs = thread_specs(type_of_thread);
    echo("specs = ",specs);
    P = specs[0]; 
    Dsupport = specs[2];//cyl interno
    vueltass= (H/P)-1;
    thread(type_of_thread, turns=vueltass);
translate([0, 0, -P / 2])
    cylinder(h=H, d=Dsupport, $fn=120);
echo("vueltas son ",vueltass);
}


  
module joint(){//piece for building others
    difference(){
        cube(size=[joint_x,joint_y,joint_z], center= true);
        cylinder(r=M3_r,h=M3_h+corr, center=true);
        }}

    
//logitech c270 camera adapter (from openflexure)
 module c270_holder(){
difference(){
         c270_camera_mount();
   translate([0, 0, -corr])     cylinder(d=((M12_r*2)+(corr*2)), h=mount_h*2);
               reflect([1,0,0]) translate([mounting_hole_x,0,0]) mounting_hole();
    }

         difference() {  
                 //threaded part
        color("lightblue")    pos_cylinder_thread(H=enganche, type_of_thread="M18x0.75-ext");//M18 porque antes era 17,4
        translate([0, 0, -enganche/2]) cylinder(d=((M12_r*2)+(corr*2)), h=mount_h*2);
       translate([0, 0, -corr])     cylinder(d=((M12_r*2)+(corr*2)), h=mount_h*2);
//               reflect([1,0,0]) translate([mounting_hole_x,0,0]) mounting_hole();
	}    }
       
//translate([ 0, 0, -50 ])       CE_c270();
// translate([0, 0, bottom])  topcase_CE_c270(); 
 module topcase_CE_c270(){
     h = 58;
    w = 25;
     difference(){
        translate([0, -13+h/2, bottom]) cube([w+wall*2, h+wall*2, c270_camera_mount_height()], center=true);
        translate([0, -13+h/2, bottom+wall/2]) cube([w+corr*3, h+corr*3, c270_camera_mount_height()+corr], center=true);  
              //exit for cable
        translate([4,20,bottom/2]) rotate([-90,0,0]) cylinder(r=3,h=99);
	}
     }
     
//ocular_cam_adapter();
module ocular_cam_adapter(){
    difference() {
    union(){ 
        translate([0,0,ocular_cam_h+ocular_in_h/2]) color("white") cylinder(r=ocular_r,h=ocular_in_h,$fn=100, center=true); //el que acepta es cylinder(r=ocular_r+corr,h=ocular_h,$fn=100, center=true);
        
   color("pink") cylinder(r=tube_r,h=ocular_cam_h,$fn=100);
        }
    translate ([0.00,0.00,-(corr)]) color ("cyan")   //joint to camera holder
        tap("M18x0.75", turns=15); 

    translate ([0.00,0.00,10]) color("green") cylinder(r=ocular_r-wall,h=ocular_in_h-wall,$fn=100);
     }    
    }

//-----------------------------------------------------knob();

//knob();  //top wheel used for focusing
module knob(){
      difference(){
      union(){
 translate([0, 0,  rodamiento_h])  
     color("cyan")  difference(){
         minkowski(){
             sphere(2);
             cylinder(r=ruedita_r, h=notch_ring_h/4,$fn=100);
         }
             for (i=[0:360/12:360]) {//number of finger holds
      rotate(i)
      translate([ruedita_r,0,-0.1])
     translate([ 0.00, dedos_d, -2 ]) color("yellow") cylinder(d=dedos_d, h=notch_ring_h);
             }}        
 translate([0, 0, 0]) color("blue") cylinder(r=rodamiento_i_r, h=rodamiento_h, $fn=100);//aca - corr*3 a un externo  porque rodam es fijo. Luego lo saque porque bailaba
 }
 color("red")translate([0,0,0])
         tap(type, turns=vueltas);
 }}   
   
 //----------------------------------------------------- main optical tube 
 
//tube_lens_threaded_ocular();
module tube_lens_threaded_ocular(){//focus control and lens holder all together
    difference() {     
        //external thread
            color("green") translate([0,0,0]) scale([0.995,0.995,1])
                pos_cylinder_thread(H=(tube_lens_totalh-corr), type_of_thread="M33x0.75-ext");
        translate([ 0,0,-corr*2]) union(){     
//         joint to ext_tube_V2
       color("red")  tap("M27x0.75", turns=30); 
            len= (lens_holder_pos+extra_doublet+corr);
            echo ("lenght is",len);
    translate([ 0.00, 0.00, corr ]) color ("blue") cylinder(r= foc_adj_r+3*corr, h= tube_lens_totalh-thread_h_cam_attach+2*corr);  
            echo(str ("total h is ",tube_lens_totalh-corr)); 
            
            //notches to stop the whole thing from rotating freely
    reflect(1,0,0) color("blue") translate([tube_r+thread_thick/2+wall/2, 0, (tube_lens_totalh)/2 ]) cube([ notch_x+corr*2, notch_y+corr*2, tube_lens_totalh+corr ], center=true);       
   color("red") translate([0, tube_r+thread_thick/2+wall/2, (tube_lens_totalh)/2 ]) cube([ notch_y+corr*2, notch_x+corr*2, tube_lens_totalh+corr ], center=true);       
  color("red") translate([0, -(tube_r+thread_thick/2+wall/2), (tube_lens_totalh)/2 ]) cube([ notch_y+corr*2, notch_x+corr*2, tube_lens_totalh+corr ], center=true);       
    }
      translate ([0.00,0.00,tube_lens_totalh-corr-ocular_h/2]) color("yellow") cylinder(r=ocular_r+corr,h=ocular_h,$fn=100, center=true);
    }     }
    
    
  //----------------------------------------ring superior
    
    
knob_ring_sup();  //top ring to hold springs
module knob_ring_sup(){//version 2023 with Nano and new library for threads
      difference(){
      union(){
 translate([0, 0,  rodamiento_h])  
     color("cyan")  difference(){
         minkowski(){
             sphere(2);
             cylinder(r=ruedita_r, h=notch_ring_h/4,$fn=100);
         }
             for (i=[0:360/12:360]) {//number of finger holds
      rotate(i)
      translate([ruedita_r,0,-0.1])
     translate([ 0.00, dedos_d, -2 ]) color("yellow") cylinder(d=dedos_d, h=notch_ring_h);
             }
        //holes for spring guide bars/screws    
              for (i=[0:360/2:360]) {//number of holes
      rotate(i)
      translate([ruedita_r/2+3,0,-10]) 
     translate([ 0, 0, -2 ]) color("white") cylinder(d=4.1, h=notch_ring_h);
             }                
             }
          
 translate([0, 0, 0]) color("blue") cylinder(r=rodamiento_i_r, h=rodamiento_h, $fn=100);//aca - corr*3 a un externo  porque rodam es fijo. Luego lo saque porque bailaba
 }
 color("white")translate([0,0,0])
         tap(type, turns=vueltas);
 }}     
 
     
  //----------------------------------------ring inferior
    

//ring_inf();  //top ring to hold springs
module ring_inf(){//version 2023 with Nano and new library for threads
      difference(){
      union(){
 translate([0, 0,  0])  
     color("cyan")  difference(){
         minkowski(){
             sphere(2);
             cylinder(r=ruedita_r*.75, h=ring_inf_h,$fn=100);
         }      
     }  }
     // holes for bolts
    for (i=[0:360/2:360]) {//number of holes
      rotate(i)
      translate([ruedita_r/2+3,0,-10])
     translate([ 0, 0, -2 ]) color("yellow") cylinder(d=4.75, h=notch_ring_h);
             }   
     //internal
     translate([0, 0, -5]) color("white")  cylinder(r=tube_r+thread_thick+corr*6, h=ring_inf_h*4,$fn=100);
 }
 
   // cyl w/ holes for bolts
    for (i=[0:360/2:360]) {//number of holes
      rotate(i)
      translate([ruedita_r/2+3,0,-9]) difference(){
     translate([ 0, 0, -2 ]) color("red") cylinder(d=10, h=notch_ring_h/2);
          translate([ 0, 0, -2 ]) color("yellow") cylinder(d=4.75, h=notch_ring_h);
      }
             }   
             
  //notches  //added corr /2 to blue and red to loosen it up a bit (25-11-20)
difference() {
   union(){ color("blue") translate([(tube_r+thread_thick/2+wall/2)+corr/2, 0, ring_inf_h/2]) cube([ notch_x, notch_y, ring_inf_h*1.4 ], center=true);      
               color("green") translate([-(tube_r+thread_thick/2+wall/2+corr/2), 0, ring_inf_h/2 ]) cube([ notch_x, notch_y, ring_inf_h*1.4 ], center=true);  
            color("red") translate([0, tube_r+thread_thick/2+wall/2+corr/2, ring_inf_h/2 ]) cube([ notch_y, notch_x, ring_inf_h*1.4 ], center=true);       
            color("cyan") translate([0, -(tube_r+thread_thick/2+wall/2+corr/2), ring_inf_h/2 ]) cube([ notch_y, notch_x, ring_inf_h*1.4 ], center=true);    }
           
           }
         
  //legs
 color("blue") translate([0,(ruedita_r/2+3), -leg_h/2]) cube([ leg_xy, leg_xy, leg_h ], center=true);      
           color("green") translate([0, -(ruedita_r/2+3), -leg_h/2 ]) cube([ leg_xy, leg_xy, leg_h ], center=true);  
      
        }     
 
  
 //----------------------------------------M12
    

//ring_M12();     
 module ring_M12(){
     difference(){
       translate([ 0, 0,ring_M12_h/2]) cylinder(r=tube_r,h=ring_M12_h, center=true,$fn=100);
        translate([ 0.00, 0.00, wall])   color ("pink")  tap("M27x0.75", turns=30);

         cylinder(r=M12_r+corr/2,h=ring_M12_h, center=true,$fn=100);
         }
     }     

//ext_M12tube();
module ext_M12tube(){//to join the main tube and hold the M12 lens 
difference() {
    union(){ 
        color("pink")  scale([0.995,0.995,1]) pos_cylinder_thread(M12ext_h+insert_h/4, "M27x0.75-ext");
       translate([0,0,(M12ext_h*3/4)/2+(M12ext_h*1/4)]) color("blue")cylinder(r=tube_r,h=M12ext_h*3/4, center=true,$fn=100);
          } 
     //rings for reflections and tope de M12 ring
  translate([0,0,M12_ring_h+(M12ext_h+insert_h)/2]) color("green") cylinder(r1=M12_r,r2=M12_ring_r,h=M12ext_h+insert_h, center=true,$fn=100);
   //internal space to trap reflections
    union(){
        translate([0,0,internal_space_h+wall]) color("red") cylinder(r=M12_r*1.5,h=internal_space_h*2, center=true,$fn=100);
        cylinder(r=M12_r+corr*2,h=ring_M12_h, center=true,$fn=100);}
                
    }}


//---------------------------------------------extension tube to hold objective

//ext_tube_4_5X();//short extension used to join to the fluor cube or straight to optical tube holding the objetive at the bottom and the achromat lens inside the tube lens on the top.
module ext_tube_4_5X(){//Bottom part for fluorescent or polarizing filters
difference() {
    union(){ 
        translate([0,0,ext_4_5_h/2]) scale([0.997,0.997,1]) color("blue") pos_cylinder_thread(rosca_h, "M27x0.75-ext");

     cylinder(r=tube_r,h=ext_4_5_h, center=true,$fn=100);}
     
    // this union is the complete internal thread, RMS for objective and anti-reflective internal surface
    translate ([0.00,0.00,-(ext_4_5_h/2)]) color("red") tap("RMS", 10);// this creates an internal RMS thread for DIN (and JIS) objectives 20.1mm / 0.7965" dia. 36 TPI, 55° Whitworth

     intlength=ext_4_5_h *1.1;
       echo (" intlength ", intlength); //how many turns?
   
         //internal not threaded
     translate ([ 0.00, 0.00,rosca_h*2])  color("yellow")cylinder(r=22/2,h=ext_4_5_h+rosca_h, center=true,$fn=100);
    
    }}
    
   
//ext_tube_40X();//short extension used to join to the fluor cube or straight to optical tube holding the objetive at the bottom and the achromat lens inside the tube lens on the top.
module ext_tube_40X(){//Bottom part for fluorescent or polarizing filters
difference() {
    union(){ 
        translate([0,0,ext_40_h/2]) scale([0.997,0.997,1]) color("blue") pos_cylinder_thread(rosca_h, "M27x0.75-ext");
     cylinder(r=tube_r,h=ext_40_h, center=true,$fn=100);}
     
    // this union is the complete internal thread, RMS for objective and anti-reflective internal surface
    translate ([0.00,0.00,-(ext_40_h/2)]) color("red") tap("RMS", 10);// this creates an internal RMS thread for DIN (and JIS) objectives 20.1mm / 0.7965" dia. 36 TPI, 55° Whitworth    
     intlength=ext_40_h *1.1;
       echo (" intlength ", intlength); //how many turns?
   
         //internal not threaded
     translate ([ 0.00, 0.00,rosca_h*2])  color("yellow")cylinder(r=22/2,h=ext_40_h+rosca_h, center=true,$fn=100);
    }}
    

//---------------------------main body 
  
//translate([ 0.00, 0.00,40]) cone_slide_fluor();
module cone_slide_fluor(){
 translate([ 0.00, 0.00,push_joint_h]){
   difference(){     
       //cone
   union(){
       color("pink")  hull(){
           translate([ 0.00, 0.00, cone_slide_h ]) 
//           pink cylinder(r=tube_r+thread_thick*4, h=1);
                                minkowski(){ 
                   cylinder(r=wall,h=1); 
                   cube(size=[filter_cube_x+wall*2,filter_cube_y+wall*2,filter_cube_z], center=true); }
 translate([0,0,-9.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=push_joint_h, center=true); 
                     cube([ push_joint_xy_V2, push_joint_xy_V2, 1 ], center=true);  };
         
                     };
      
       //slide holder  
//                    color("blue") minkowski(){ 
//                 translate([ 0.00, 0.00, -1/2 ])   cylinder(r=wall,h=1, center=true);                 translate([ 0.00, 0.00, -1/2 ])      cube([ push_joint_xy_V2, push_joint_xy_V2, 1 ], center=true);}
 }
  translate([ 0, 0, -wall*4 ])  color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h+corr , center=true); 
                    cube([ push_joint_xy_V2-wall, push_joint_xy_V2-wall,  1 ], center=true); 
        }
        
        //window LED in       
          translate([ 0.00, 30, cone_slide_h/2 ]) cube([filter_cube_x+wall,filter_cube_x+wall,80], center=true);
          
       //internal                 
   color("green")  hull(){ 
       translate([ 0.00, 0.00, cone_slide_h ])
       minkowski(){ 
                   cylinder(r=wall,h=1); 
                                    cube(size=[filter_cube_x,filter_cube_y,filter_cube_z], center=true); }
                                      translate([ 0, 0, -wall*10 ])  color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h+corr , center=true); 
                    cube([ push_joint_xy_V2-2*wall, push_joint_xy_V2-2*wall,  1 ], center=true); 
        }

                                    };
                            
//     arc window (better for printing without support)
                   translate([ 0.00, 0.00, slide_holder_z/2  -slide_x ]) color("red")   rotate([ 90, 0, 0 ]) cylinder( r=slide_x*1.2,h= slide_y+wall , center=true);     
       
                   }
                   
  translate([ 0.00, 0.00, htt_h+cone_slide_h]) difference(){
    translate([ 0, 0, 0 ]) color("blue")  cylinder(r=rodamiento_e_r+thread_thick, h=rodamiento_h,$fn=100);
    translate([0, 0, thread_thick]) color("pink")  cylinder(r=rodamiento_e_r+corr, h=rodamiento_h,$fn=100);
    translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);
    }                 
                    
translate([ 0.00, 0.00,cone_slide_h]) 
  union(){
      difference(){
        color("grey")  sequential_hull(){ 
            translate([ 0.00, 0.00, htt_h ]) cylinder(r=rodamiento_e_r+thread_thick,h=1); 
//            translate([ 0.00, 0.00, htt_h*0.7 ])   cylinder(r=rodamiento_e_r*0.8,h=1); 
//            cylinder(r=tube_r+thread_thick*4, h=1);   
            translate([ 0.00, 0.00, htt_h*0.5 ])    minkowski(){ 
                   cylinder(r=wall,h=1); 
                                    cube(size=[filter_cube_x+wall*2,filter_cube_y+wall*2,filter_cube_z], center=true); }     
                                    };
//          cylinder(r=tube_r+thread_thick*4, h=htt_h,$fn=100);
                translate([0, 0, -corr]) color("white")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*4,$fn=100);}
                
             //notches  //added corr /2 to blue and red to loosen it up a bit (25-11-20)
difference() {
   union(){ color("blue") translate([(tube_r+thread_thick/2+wall/2)+corr/2, 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);      
               color("green") translate([-(tube_r+thread_thick/2+wall/2+corr/2), 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);  
            color("red") translate([0, tube_r+thread_thick/2+wall/2+corr/2, htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);       
            color("cyan") translate([0, -(tube_r+thread_thick/2+wall/2+corr/2), htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);    }
           
            hull(){ 
       translate([ 0.00, 0.00, 0 ])
       minkowski(){ 
                   cylinder(r=wall,h=1); 
                                    cube(size=[filter_cube_x,filter_cube_y,filter_cube_z], center=true); }
                                      translate([ 0, 0, -wall ])  color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h+corr , center=true); 
                    cube([ push_joint_xy_V2-2*wall, push_joint_xy_V2-2*wall,  1 ], center=true); 
        }      };   }
                
translate([ 0.00, 0.00, htt_h]) difference(){
    translate([ 0, 0, 0 ]) color("blue")  cylinder(r=rodamiento_e_r+thread_thick, h=rodamiento_h,$fn=100);
    translate([0, 0, thread_thick]) color("pink")  cylinder(r=rodamiento_e_r+corr, h=rodamiento_h,$fn=100);
    translate([0, 0, -corr]) color("green")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);
    } }     }       }
       

//---------------------------------------------push version liso deprecated....
    
     tube_h= ext_bot_len+extra_doublet*2+int_focal_corr_h; 
   //push version 
//  translate([0,0,0]) tube_lens_liso();
module tube_lens_liso(){//focus control and lens holder all together
    difference() {     
        //external thread
            color("green") translate([0,0,0])
                cylinder(r=tube_r, h=(tube_h-corr));
        translate([ 0,0,-corr*2]) union(){     
//         joint to ext_tube_V2
       color("red")  tap("M27x0.75", turns=30); 
            len= (lens_holder_pos+extra_doublet+corr);
            echo ("lenght is",len);

    translate([ 0.00, 0.00, corr ]) color ("blue") cylinder(r= foc_adj_r+3*corr, h= ext_bot_len+extra_doublet*2+int_focal_corr_h-thread_h_cam_attach+2*corr);
     
            echo(str ("total h is ",tube_h-corr)); 
    }
     translate ([0.00,0.00,tube_h-corr+insert_h-ocular_h]) color("yellow") cylinder(r=ocular_r+corr,h=ocular_h,$fn=100, center=true);
    }     }
   
    
   
//------------------------------------------ring holders and tools for screwing/unscrewing it

    retainer_h=4;
translate([0,0,-10])  rotate(90,0,0) lens_holder_ring_V2();
module lens_holder_ring_V2(){//internal lens retainer ring mas chico
difference() {
    color("red") scale([0.985,0.985,1]) pos_cylinder_thread(retainer_h, "M27x0.75-ext");                   
    translate([ 0.00, 0.00, -corr*2 ])cylinder(r=int_r-corr*2,h=retainer_h*2+corr*2);
    cube([ 1, 100, 2 ], center=true);
}}      


    
// lens_holder_ring_tool();
module lens_holder_ring_tool(){//to rotate ring
tot_len=2.5;
difference() {
   hull(){
        translate([ 0.00, 0.00, 0 ])  cube([(M27_r*2)-(corr*8), 1, 1], center=true) ;
    translate([ 0.00, 0.00, M27_r ])   cylinder(r=(M27_r/2)-(corr*10), h=1, center=true); 
    }
    translate([ 0.00, 0.00, -M27_r/2 ]) sphere(r=(M27_r)-(corr*3));
}
 translate([ 0.00, 0.00, M27_r*2 ])   cylinder(r=(M27_r/2)-(corr*10), h=M27_r*2, center=true); 
}      

//to use as a template for optical tubes 
// translate([0,0,00]) generic_tube_lens();
module generic_tube_lens(){//focus control and lens holder all together
    difference() {     
        //main body
            color("green") 
                cylinder(r=33/2, h=62);         
//        internal thread of 1 inch
    translate([ 0,0,wall]) color("lightblue")  tap("M27x0.75", turns=300); 
//            len= (lens_holder_pos+extra_doublet+corr);
//            echo ("lenght is",len); 
         translate([0,0,-c_mount_h]) color("lightblue") cylinder(r=internal_path_r,h=100);
}}  

    
  

