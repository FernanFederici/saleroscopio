/*
BrewerMicro project : https://gitlab.com/nanocastro/microbrew
 (c) Fernán Federici,  Nano Castro, July 2020    
Released under the CERN Open Hardware License       
*/

//+++++++++ Librerias ++++++++/////
/* Modified from Openflexure project
https://gitlab.com/openflexure/openflexure-microscope-snesclient*/
include <./librerias/logitech_c270_FF.scad>; 
include <./librerias/logitech_c920.scad>;
include <./librerias/sample_clips.scad>; 
use <./librerias/illumination.scad>;
include <./librerias/microscope_parameters.scad>;

/* Librería de roscas
https://github.com/adrianschlatter/threadlib */
use <./librerias/threadlib/threadlib.scad>
echo ("threadlib version: ", __THREADLIB_VERSION());
/* Librería de rodamientos*/
include<./librerias/bearings.scad>
/* Libreria cable clip */
include<./librerias/CableClipV2.scad>


//////+++++++++ Parámetros ++++++++/////

// Cámara
camera_type="c270";//camera_type can be c270 or c920

// Medida de las roscas (ver librería)
type = "M20x1";//Tornillo M20 con paso de 1mm
//Douter = thread_specs(str(type, "-int"))[2] * 1.2;//diametro exterior de tuercas
Douter = thread_specs(str(type, "-int"))[2] * 1.2;

/// Ruleman (ver librería para modelos disponibles)
ruleman=625;
h_rul=h_625;

corr=0.2;
tot_len=55;//Largo total del tubo sobre la base
sample_thread_h=30;//Largo de la rosca en el sample holder
M12_h=12;//Altura lente M12
M12_ring_h=4;// Altura base lente M12
M12_r=14/2;//external size
slide_y=95; // Dimensiones para slide
slide_x=95;
slide_z=2;
base_z=10;
$fn=60;

////////+++++++++ Renders ++++++++////////
//base(camera_type);
//tubo();
//translate([0,0,tot_len*5/4]){
//sample_holder();
//translate([0,0,M12_h+slide_z])
//bm2l_light();
//}

//sample_clip for microscope slide
//for(a=[0,180]) rotate([0,-90,a]) translate([7/2,-10,-7+1])
//sample_clip([0,20,-1], w=7, roc=7); 

// cable clip 
//translate([0,0,b_clip/2])
//rotate([0,90,0])
//clip(l,h,b_clip,d1,d2);


//+++++++++ Código de las partes ++++++++/////

///// Base

module base(camera_type){ 
        if(camera_type=="c270"){
       union(){c270_camera_mount();            translate([-w/2-b_clip/2+0.5,-w/2+h/2,-10+b_clip/2-0.35])
rotate([90,90,0])
clip(l,h,b_clip,d1,d2);}
            }
         
        if(camera_type=="c920") {
       c920_camera_mount();}
     
turns_base=round(tot_len/4);
translate([0,0,5])
nut(type, turns_base, Douter);
}

///// Tubo optico

module tubo(){
turns_base=round(tot_len/4);
translate([0,0,tot_len/3])
difference(){
turns_tube=round(tot_len);
bolt(type, turns_tube);

translate([ 0, 0, turns_tube-  M12_ring_h ]) color("red") cylinder(r=(M12_r+corr*2)*1.03,h= M12_ring_h+5);//where M12 lens sits
translate([0,0,turns_tube/2])color ("green")cylinder(r=beam_r*1.03,h=turns_tube*2,center=true);
}
}

/// Sample_holder
module sample_holder(){
turns_sample=sample_thread_h-M12_h;
union(){
difference(){
 union() {
    
cylinder(r1=Douter/2,r2=26,h=M12_h+slide_z);
            translate([ 0.00, 0.00, M12_h ]) minkowski(){
              sphere();
              cube([ slide_x, slide_y/2, slide_z ], center=true); }
        
        translate([ 0.00, 0.00, M12_h]) minkowski(){sphere(); 
    #cube([ slide_x/3, slide_y, slide_z], center=true);         
            }
}

cylinder(r1=Douter/2-1.6,r2=24,h=M12_h+slide_z);

    #translate([0,slide_x/2-b/2,-slide_z-1+M12_h]) cylinder(d=a+corr,h=15);
             
    translate([0,-slide_x/2+b/2,-slide_z-1+M12_h]) cylinder(d=a+corr,h=15);
        
    translate([ 0.00, 0.00, M12_h ]) minkowski(){ sphere(); cube([ 25, 25, slide_z ], center=true); }
    
    #translate([slide_x/4,-slide_y/6,M12_h ])
    trylinder_selftap(3, slide_z*2, center=true);
    
    #translate([-slide_x/4,-slide_y/6,M12_h])
    trylinder_selftap(3, slide_z*2, center=true);
    
    #translate([slide_x/3,-slide_y/6,M12_h ])
    trylinder_selftap(3, slide_z*2, center=true);
    
    #translate([-slide_x/3,-slide_y/6,M12_h ])
    trylinder_selftap(3, slide_z*2, center=true);
    
    #translate([slide_x/2-slide_z*2,0,M12_h])
    trylinder_selftap(3, slide_z*2, center=true);
    #translate([-slide_x/2+slide_z*2,0,M12_h])
    trylinder_selftap(3, slide_z*2, center=true);
}
}
translate([0,0,-turns_sample])           
nut(type, turns_sample, Douter);
}


//// Iluminación 
light_dist=15;
col_base=10;
lens_assembly_z=0;
base_r=11;

bm2l_light_c270();
//bm2l_light();
module bm2l_light_c270(){
difference(){
    union(){
translate([ 0.00, 0.00, light_dist+slide_z+lens_assembly_z+d])
rotate([180,0,0]) color("cyan") scale([ 1.02, 1.02, 1 ])c270_condenser(); //adjust scale if you have issues accommodating the lens
light_arm();
    mirror([0,1,0]){light_arm();}
}
translate([ 0.00, 0.00, light_dist+slide_z+lens_assembly_z+d+h_685])
color("blue")bearing(ruleman);
}
}
//bearing(ruleman);
c270_cond_xy=14;
c270_cond_screw_d=4.7;//ancho patitas paratornillos
//translate([ -0.61, 10.20, 53.57 ])c270_condenser();
module c270_condenser(){
    difference(){
        tall_condenser();
        //c270 condenser shape to substract
      # translate([ 0, 0, 8 ])union(){
            cube([ c270_cond_xy+corr,c270_cond_xy+corr , 15], center=true);
            translate([ 0,c270_cond_xy , 0])cube([ c270_cond_screw_d+corr,c270_cond_xy+corr , 15], center=true);
            translate([ 0,-c270_cond_xy , 0])cube([ c270_cond_screw_d+corr,c270_cond_xy+corr , 15], center=true);
         translate([ 0,0 , 5])cube([ c270_cond_xy*2,c270_cond_xy*2 , 15], center=true);
            }
            
    }
    
    }


//bm2l_light();

module bm2l_light(){
difference(){
    union(){
translate([ 0.00, 0.00, light_dist+slide_z+lens_assembly_z+d])
rotate([180,0,0]) color("cyan") scale([ 1.02, 1.02, 1 ])tall_condenser(); //adjust scale if you have issues accommodating the lens
light_arm();
    mirror([0,1,0]){light_arm();}
}
translate([ 0.00, 0.00, light_dist+slide_z+lens_assembly_z+d+h_685])
color("blue")bearing(685);

translate([ 0.00, 0.00, light_dist+slide_z+lens_assembly_z+d+h_rul])
#bearing(ruleman);
}
}

module light_arm(){
difference(){
union(){ 
translate([-col_base/2,slide_x/2-col_base,0])cube([col_base,col_base,light_dist+slide_z*2]);
translate([-col_base/2,base_r/2,light_dist+slide_z+lens_assembly_z+d])
color("white")cube([col_base,slide_x/2-base_r/2,col_base]);
}
translate([0,slide_x/2-col_base/2,slide_z*2])
rotate([90,0,180])
color("red")nuttrap();
color("green")hull(){translate([0,slide_x/2-col_base/2,-slide_z/2])cylinder(d=a+corr,h=slide_z*5);
translate([0,slide_x/2,-slide_z/2])cylinder(d=a+corr,h=slide_z*5);
}
}
}


////// Brazo luz
//base_support_w=25;
//base_support_h=10;
//
//#translate([-w/2-base_support_h,0,-c270_camera_mount_height()*2]) minkowski(){
//        sphere();
//        cube([base_support_h, base_support_w, c270_camera_mount_height()*2]);
//}





/// Impresion de bolt y nut para prueba

//translate([30,0,0])
//bolt(type, 25);
//nut(type, 15, Douter);





///// Fijación con tornillos para la luz //////
//Set Screw Diameter
Set_Screw_Diameter=2.9; //[2:10]
//Set Screw Nut Diameter
Set_Screw_Nut_Diameter=5.5; //[4:20]
//Set Screw Nut Thickness
Set_Screw_Nut_Thickness=2; //[2:10]

a=Set_Screw_Diameter; //[2:10]
//Set Screw Nut Diameter
b=Set_Screw_Nut_Diameter; //[4:20]
//Set Screw Nut Thickness
c=Set_Screw_Nut_Thickness; //[2:10]

module nuttrap(){
translate([0,(c+1)/2,0])rotate([90,0,0])hexagon(c+1,(b+1)/2);
translate([0,0,(b*3)/2])cube([b+1,c+1,b*3],center = true);
}

module reg_polygon(sides,radius)
{
  function dia(r) = sqrt(pow(r*2,2)/2);  //sqrt((r*2^2)/2) if only we had an exponention op
  if(sides<2) square([radius,0]);
  if(sides==3) triangle(radius);
  if(sides==4) square([dia(radius),dia(radius)],center=true);
  if(sides>4) circle(r=radius,$fn=sides);
}

module hexagonf(radius)
{
  reg_polygon(6,radius);
}

module hexagon(height,radius) 
{
  linear_extrude(height=height) hexagonf(radius);
}

