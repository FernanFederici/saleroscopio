use <threads.scad>
use <utilities.scad>
use <logitech_c270.scad>
include <picam_2_push_fit.scad>         
use <./librerias/illumination.scad>
use  <./librerias/BM2L_v2.scad>


tube_r=29.85/2;//external size
tot_len_bottom=6+40+5;//total length of the ext_tube section
M27_r=27/2; //M27 threads
corr=0.2;// used for printing imperfections

lens_holder_male_len=3.5; //length of male cylinder end that goes inside top part and holds the lens against the notch of top part 


//--------------test of old threads----------------
//translate([0,0,-0]) test_print_femaleM27();
//translate([0,0,10]) test_maleM27();
    
//--------------test of new threads----------------

//M27x0_75();
//M27xtest();




//------------old modules----------------

module test_print_femaleM27(){//Bottom part merges DIN_C_mount, 40_ext and the bottom half of Lens_mount
//render() {
tot_len_bottom=10;//total len of a single piece bottom
difference() {
    cylinder(r=tube_r,h=tot_len_bottom,$fn=100);
    translate ([0.00,0.00,-(tot_len_bottom/3)]) color("red") 
    english_thread (diameter=(((M27_r*2)+(corr*0))/25.4), threads_per_inch=32, length=(tot_len_bottom *1.1)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal rough antireflective surface 
 }}


module test_maleM27(){//test printing resolution and fitting between male and female threads 
    tot_len_bottom=10;
        translate([0,0,0]) color("green") english_thread (diameter=((M27_r*2)-3*corr)/25.4, threads_per_inch=32, length=(lens_holder_male_len*1.5)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//this size (printed in a Zortrax M200 in high quality Z-ABS) fits perfectly in female Edmunds optics metallic parts 
    //     cylinder(r=tube_r,h=tot_len_bottom/2,$fn=100);
     }
     
     //------------new modules----------------
     
use <./threadlib/threadlib.scad>

//echo ("threadlib version: ", __THREADLIB_VERSION());

type = "M33x0.75";
type_test="M33x0.75-ext";//"G1/2-ext";
vueltas=17;
Douter = thread_specs(str(type, "-int"))[2] * 1.2;

echo ("Douter: ", Douter);
   

module M27x0_75(){
difference(){
cylinder(d=Douter,h=10,center=true);
echo(thread_specs(str(type, "-ext")));
color("blue")translate([0,0,-vueltas*0.75/2])
        tap(type, turns=vueltas);
//#translate([0,0,20])
//thread("M27x0.75",turns);
}

color("red") translate([0,0,0])
bolt(type, turns=vueltas);}

test_def_h();//como saber la altura para usarla a priori en diseños limitados por dimensiones ya dadas
module test_def_h(){
    H=25;//define the height internally in the module
    specs = thread_specs(type_test);
    P = specs[0]; 
    Dsupport = specs[2];
    vueltass= (H/P)-1;
    thread(type_test, turns=vueltass);
translate([0, 0, -P / 2])
    cylinder(h=H, d=Dsupport, $fn=120);
echo("vueltas son ",vueltass);
}

module test_sacar_h(){
    thread(type_test, turns=vueltas);

specs = thread_specs(type_test);
P = specs[0]; 
Dsupport = specs[2];
H = (vueltas + 1) * P;
translate([0, 0, -P / 2])
    cylinder(h=H, d=Dsupport, $fn=120);
    echo("the height is ",H);
    vueltass= (H/P)-1;
echo("vueltas son ",vueltass);
}









//change to XXX 
module M27xtest(){
type1="M27x0.75";
difference(){
#cylinder(d=Douter,h=10,center=true);
echo(thread_specs(str(type1, "-ext")));
translate([0,0,-vueltas*0.75/2])
        tap(type1, turns=vueltas);
//#translate([0,0,20])
//thread("M27x0.75",turns);
}

translate([0,0,0])
bolt(type1, turns=vueltas);}