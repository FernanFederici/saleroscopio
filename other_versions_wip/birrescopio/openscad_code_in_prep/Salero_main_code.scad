/*This project is based on:
1) optical designs made for Comunicaciones Especulativas: http://interspecifics.cc/comunicacionesespeculativas/ and https://osf.io/c542q/ - which recreates -as a 3D printed version- the optical commercial pieces used by Alexandre Kabla and his team for the openlabtools microscope (http://openlabtools.eng.cam.ac.uk/Instruments/Microscope/Optics/)


2) <picam_2_push_fit.scad> from OpenFlexure Microscope: Raspberry Pi Camera v2 push-fit mount (c) Richard Bowman, January 2016 Released under the CERN Open Hardware License  

3) https://github.com/adrianschlatter/threadlib 

(before I used <threads.scad> from http://dkprojects.net/openscad-threads/ but it takes ages and some computers would never render designs...)

All this code is licensed under the Creative Commons - Attribution  (CC BY 3.0)
Fernán Federici and Nano Castro 2020
*/


use <utilities.scad>
use <logitech_c270.scad>
include <picam_2_push_fit.scad>         
use <./librerias/illumination.scad>
use  <./librerias/BM2L_v2.scad>
//include <./librerias/logitech_c270_FF.scad>; 

////---- new thread library -----------
use <./threadlib/threadlib.scad>
//use <threads.scad> //not in use anymore


corr=0.2;// used for printing imperfections when one piece has to go into another; highly reccommended
sunny_space=6;
wall=2.5;
base_xy=16;
screw_d=21;//distance between screw holes
screw_r=2/2;
screw_hold_r=2;
M12_r=12/2;
mount_h = 10;
int_r=22/2;// internal r of cylinder according to edmund optics
RMS_r=(0.8*25.4)/2; //10,16 mmm of r for DIN (and JIS) objectives that uses RMS, this is about 20.3mm (with thread step of about 0.7mm).
tube_r=29.85/2;//external size
c_mount_r=25.4/2; //for 1 inch standard C-mount diameter
internal_lens_hol=26; //where the Comar lens stays
RMS_mount_h=6; 
c_mount_h=4;
M27_r=27/2; //M27 threads
lens_holder_male_len=3.5; //length of male cylinder end that goes inside top part and holds the lens against the notch of top part 
focus_reg_len=19;// thread for focus adjustment
M3_brass_push_h=4;//for M3 screws
M3_brass_push_w=4;//for M3 screws
tot_len_top=15+5+10+11.3+4;//total len of a single piece A
ext_bot_len=11.3+10+5;
aper_int_h=22.5; //h of internal space 
foc_adj_r=21.5/2;//width of thread for focus control
int_focal_corr_h=16;//total length to adjust focus without crashing to lens of top bottom piece
int_focal_corr_r=18/2;
lens_holder_pos=8.5; //distance of lens holder notch from bottom end of top part 
tot_len_focus_adjust_tube=4+15+16;//16mm to not crash the lens inside the top bottom part
tot_len_bottom=6+40+5;//total length of the ext_tube section
fluo_top_h=26;
pol_ring_h=10;
fluo_bottom_h=25;//ext_fluo_bottom height
sens_x=8.5;
filter_cube_x=40;
filter_cube_y=40;
filter_cube_z=24;
x_triangle=19;//to have hipotenuse of >25
y_triangle=19;//to have hipotenuse of >25
collimated_r=x_triangle/2-x_triangle*0.05;
M3_r=3/2;
M3_h=10;
joint_x=5;
joint_y=5;
joint_z=2;
filter_cube_top_z=10;  
extra_doublet=10; 
MM_h=15;
base_h=6;
mounting_hole_x = 8.25;
thread_h_cam_attach=10;//height of thread holding the RPI adapter
$fn=60;
thread_thick=2; //add thickness for external thread used in focus adjustment
notch_x=thread_thick*2;//notch for avoiding rotation
notch_y=2;//notch for avoiding rotation
htt_h=45;//height of notches in holder_of_threaded_tube module
notch_ring_r=tube_r+thread_thick*4;
notch_ring_h=25;    
rodamiento_i_r=40/2;
rodamiento_e_r=68/2;
rodamiento_h=15;//https://www.bearingkingdom.com/ball-bearings/deep-groove-radial-ball-bearings/6008_ZZ_C3_KOYO_Deep_Groove_Bearing___40x68x15mm.html
dedos_d=18; 
ruedita_r=tube_r+thread_thick*14;             
cono_h=50; //altura cono     
slide_y=75;
slide_x=26;
slide_z=1;      
sample_ring_h=20; //base ring for XY sample manipulation
push_joint_h=20; //union entre cono y base de iluminacion    
push_joint_xy=40;        
cone_slide_h=cono_h+20;//mas alto para meter 100X  
slide_holder_z=slide_z*23;
lighting_base_h=70;
pol_room_h=20;
pol_r=12.5;
pol_h=2;
holder_x=8;
holder_y=3;
holder_z=2;
cyl_h=8 ;
big_out_r=38/2;
cond_lens_r=45/2;
thick=3;
cond_h=25;
lens_hold_h=10;
lens_stop_r=43/2;
ring_h=2.5;
neck_length=190;
neck_holder_x=20;
neck_holder_y=20;
neck_holder_z=20;
neck_screw_d=7;
decr_h=5;//total height of decr siloutte      
dent_h=1;
push_joint_xy_V2=push_joint_xy*1.7;
pol_hold_r=push_joint_xy_V2/2;
push_joint_xy_adapt= push_joint_xy_V2*0.65;
base_LED_h=30;
ocular_r=23.2/2;
ocular_h=int_focal_corr_h+7;
     
insert_h=20; // esto dice cuanto de largo le doy al tubo que luego entra el ext_tube part
ocular_cam_h=4;
ocular_in_h=22; //distancia in del ocular amazon 18mm
M12ext_h=20;
M12_ring_r=14/2;
M12_ring_h=2;//altura de borde M12
internal_space_h=M12ext_h+insert_h/4-wall*3-M12_ring_h;
ring_M12_h=8;
base_h_led_holder=base_h*3;     
eye_adapt_h=20;

// new param
pcbx = 40.1+0.2;  // pcb long dimension
pcby = 27.2+0.2;  // pcb shorter dimension
pcbz = 1.05+0.2;  // pcb thickness

lensd = 7.1+0.5;  // lens dia
lensx = 29.5; //coord x del centro lente
lensy = pcby /2.0; // coord y del centro lente
lensz = 5.4;  // bottom of pcb to base of lens
lensh = 2.25+0.2; // height of lens + extra
cambx = 8.65+0.4; // clearance needed for body of camera
camby = 8.65+0.4;
borde=4;
enganche=6;//thereaded part that attaches the camera holder to the opetical tube
excess_obj=24; 
   
M470L5_r=30.5/2;//radious of LED module from thorlabs (used to benchmark the whole fluor cube and path)
ACL25416U_r=25.4/2;//width of aespherical condenser lens put in front of the fluor LED
BFL= 7.3; //BACK FOCAL LENGTH of ACL25416U
front_led=22;//distance from cable to LED head in Thorlabs M470L5
    
type = "M33x0.75";//"M27x0.75"; //main thread type of the optical tube and knob
//type_test="M33x0.75-ext";//"G1/2-ext";
vueltas=37;
Douter = thread_specs(str(type, "-int"))[2] * 1.2;
diameter_internal= ((tube_r+thread_thick)*2+corr*4);//para saber el ancho interno antiguo
echo ("diametro interno= ", diameter_internal);

tubelens_h= ext_bot_len+extra_doublet*2+int_focal_corr_h-corr;    
echo ("tubelens height= ", tubelens_h);


//The order of modules - from top to bottom - is:

//--------------------------------1) camera adapters

        //using now:
//        ESP32camera();//for camera ESP32cam
//   c270_adapter();//for logitech c270 camerra


        //other options:
//        cam_adapter_v2(); //for RPI
//        topcase_CE_c270(); 

//---------------------------------2) focus knob

//knob_V2();

//------------------------3) main optical tube

//tube_lens_V2();//new library 2023

//--------------------------3b) fluor cube

// ext_tube_MM();//male male to join to filter cube  
// translate([0,0,80]) rotate([0,180,0]) filter_cube_top();
//translate([0,0,80]) filter_cube_bott();
// translate([0,40,80 -y_triangle/2]) LED_in();
// translate([ 120.00, 40+filter_cube_y/2+ wall,80 -y_triangle/2 ]) M470L5_holder();


//----------------------------4)extension tube to hold objective

//ext_tube_V2();//short extension used to join to the fluor cube or straight to optical tube holding the objetive at the bottom and the achromat lens inside the tube lens on the top.

//---------------------------5) main body

//cone_slide_fluor(); //used for fluor
//cone_slide();//being used lately for not-fluor (it needs an update)


//--------------------------6) base

//      base_fluor();//for sample holding

//--------------------------7)LED holder in base

//led_holder();

//--------------------------8) polarizing extension

//pol_ext_V4();//if you want a polarizer filer for crystals

//--------------------------9) polarizer holder

//pol_ring(); 
//pol_ext_V4();

//lens_holder_ring_V2;

//----------------------------------------------generic modules used to build others
    
//pos_cylinder_thread(20,"M33x0.75-ext");//H define the height in mm and type of thread
module pos_cylinder_thread(H, type_of_thread){
    specs = thread_specs(type_of_thread);
    P = specs[0]; 
    Dsupport = specs[2];
    vueltass= (H/P)-1;
    thread(type_of_thread, turns=vueltass);
translate([0, 0, -P / 2])
    cylinder(h=H, d=Dsupport, $fn=120);
echo("vueltas son ",vueltass);
}

  
module dic_house(){ //piece for building others
   translate([ 0.00, -y_triangle/2, 0.00 ]) rotate([ 0.00, 90.00, 0.00 ]) linear_extrude(height = 37, center = true, convexity = 10, twist = 0, slices = 20, scale = 1.0) {polygon( points=[[0,0],[0,y_triangle],[x_triangle,0]] );}}
   
module joint(){//piece for building others
    difference(){
        cube(size=[joint_x,joint_y,joint_z], center= true);
        cylinder(r=M3_r,h=M3_h+corr, center=true);
        }}



//-----------------------------------------------------ESP32camera

// ESP32camera();
 module ESP32camera(){
  //threaded part
translate([lensx+borde/2, (pcby+borde)/2, enganche]) difference(){  
           
           pos_cylinder_thread(H=enganche, type_of_thread="M18x0.75-ext");//M18 porque antes era 17,4
//   color("lightblue") english_thread (diameter=((int_focal_corr_r*2)-(3*corr))/25.4, threads_per_inch=32, length=(M3_brass_push_h/1.3+corr)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
        translate([0, 0, -enganche/2]) cylinder(d=((M12_r*2)+(corr*2)), h=mount_h*2);
//               rotate([ 0.00, 0.00, -45.00 ]) reflect([1,0,0]) translate([mounting_hole_x,0,0]) mounting_hole();
    }
    //case part
         difference() {  
             cube([pcbx+borde,pcby+borde,pcbz+borde*1.3]);
             translate([borde/2,borde/2, 0])ESP32camera_pos(); 
     }}


     
//ESP32camera_pos();   
module ESP32camera_pos(){ // generate a shape model of the camera upper surface and pcb
// this gets subtracted from a solid base to form a pocket that
// holds the pcb and camera in place.
  hull()
  {
    translate([lensx-cambx/2,lensy-camby/2,0]) cube([cambx,camby,lensz]);   // camera body
    translate([16.2,1,0]) cube([20,2,3.5]);                                 // room for pins
    translate([16.2,24.0,0]) cube([20,2,3.5]);                              // room for pins
    translate([25.2,pcby/2-7.5,0]) cube([15.0,15.0,3.0]);                   // sdcard slot
    translate([9.3,0.4,0]) cube([7,3.5,2.0]);                               // led + xsistor
    translate([8,4.8,0]) cube([9.4,16.5,3.5]);                              // cam connector
    translate([17.3,3.5,0]) cube([7.75,19.75,3.1]);                         // cable + parts
    translate([10.0,22.3,0]) cube([6.5,3.5,2.2]);                           // reg + parts
  }
  cube([pcbx,pcby,pcbz]);
  //translate([1,pcby-4,pcbz]) rotate([0,0,-90]) linear_extrude(0.5) text("ESP32-CAM",2.5);
  #  translate([lensx,lensy,lensz]) cylinder(h=lensh, d=lensd);  // lens
  //ranslate([lensx,lensy,lensz+lensh]) cylinder(h=6, d1=lensd, d2=3*lensd); // lens flare
  // below is optional... only needed if you need to put an sd card in the camera
  //translate([40.0,pcby/2-7.5,pcbz]) cube([15.0,15.0,3.0-pcbz]);             // sdcard slot access
}   

      
//  translate([ 0, 20, -50 ]) cam_adapter_v2();   
module cam_adapter_v2(){
    difference() {
                union() {
                    //threaded part
                    translate([0,0,M3_brass_push_h/1.5 + base_h-corr])       
   color("lightblue") 
                  difference(){  
           
           pos_cylinder_thread(H=enganche, type_of_thread="M18x0.75-ext");//M18 porque antes era 17,4
//   color("lightblue") english_thread (diameter=((int_focal_corr_r*2)-(3*corr))/25.4, threads_per_inch=32, length=(M3_brass_push_h/1.3+corr)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
        translate([0, 0, -enganche/2]) cylinder(d=((M12_r*2)+(corr*2)), h=mount_h*2);
//               rotate([ 0.00, 0.00, -45.00 ]) reflect([1,0,0]) translate([mounting_hole_x,0,0]) mounting_hole();
    }
//                    english_thread (diameter=((int_focal_corr_r*2)-(3*corr))/25.4, threads_per_inch=32, length=(M3_brass_push_h/1.5+corr)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
				translate([0,sunny_space/2,base_h/2+wall/2])
					color("green")  cube([base_xy, base_xy+sunny_space, base_h+wall], center = true);
				color("Red") translate([screw_d/2, 0, 0])
					cylinder(r = screw_hold_r, h = base_h/2);
				color("Red") translate(-[screw_d/2, 0, 0])
					cylinder(r = screw_hold_r, h = base_h/2);
				color("Red") translate([0,0,base_h/2/2])
						cube([screw_d, 4, base_h/2], center = true);
		}
		translate([0,sunny_space/2,base_h/2-wall])
			color("blue") cube([base_xy - wall, base_xy+sunny_space - wall, base_h*2], center = true);
		translate([screw_d/2, 0, 0])
			cylinder(r = screw_r, h = base_h);
		translate([-screw_d/2, 0, 0])
			cylinder(r = screw_r, h = base_h);
	
        translate([0, 0, base_h-corr])     cylinder(d=((M12_r*2)+(corr*2)), h=mount_h*2);
	}}
    
//logitech c270 camera adapter (from openflexure)
 module c270_adapter(){
difference(){
         c270_camera_mount();
   translate([0, 0, -corr])     cylinder(d=((M12_r*2)+(corr*2)), h=mount_h*2);
               reflect([1,0,0]) translate([mounting_hole_x,0,0]) mounting_hole();
    }

         difference() {  
                 //threaded part
        color("lightblue")    pos_cylinder_thread(H=enganche, type_of_thread="M18x0.75-ext");//M18 porque antes era 17,4
//   color("lightblue") english_thread (diameter=((int_focal_corr_r*2)-(3*corr))/25.4, threads_per_inch=32, length=(M3_brass_push_h/1.3+corr)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
        translate([0, 0, -enganche/2]) cylinder(d=((M12_r*2)+(corr*2)), h=mount_h*2);
//               rotate([ 0.00, 0.00, -45.00 ]) reflect([1,0,0]) translate([mounting_hole_x,0,0]) mounting_hole();

//             english_thread (diameter=((int_focal_corr_r*2)-(3*corr))/25.4, threads_per_inch=32, length=(M3_brass_push_h/1.3+corr)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
       translate([0, 0, -corr])     cylinder(d=((M12_r*2)+(corr*2)), h=mount_h*2);
//               reflect([1,0,0]) translate([mounting_hole_x,0,0]) mounting_hole();
	}    }
     
     
//translate([ 0, 0, -50 ])       CE_c270();
// translate([0, 0, bottom])  topcase_CE_c270(); 
 module topcase_CE_c270(){
     h = 58;
    w = 25;
     difference(){
        translate([0, -13+h/2, bottom]) cube([w+wall*2, h+wall*2, c270_camera_mount_height()], center=true);
        translate([0, -13+h/2, bottom+wall/2]) cube([w+corr*3, h+corr*3, c270_camera_mount_height()+corr], center=true);  
              //exit for cable
        translate([4,20,bottom/2]) rotate([-90,0,0]) cylinder(r=3,h=99);
	}
     }


//-----------------------------------------------------knob_V2();

//knob_V2();  //top wheel used for focusing
module knob_V2(){//version 2023 with Nano and new library for threads
      difference(){
      union(){
 translate([0, 0,  rodamiento_h])  
     color("cyan")  difference(){
         minkowski(){
             sphere(2);
             cylinder(r=ruedita_r, h=notch_ring_h/4,$fn=100);
         }
             for (i=[0:360/12:360]) {//number of finger holds
      rotate(i)
      translate([ruedita_r,0,-0.1])
     translate([ 0.00, dedos_d, -2 ]) color("yellow") cylinder(d=dedos_d, h=notch_ring_h);
             }}
          
 translate([0, 0, 0]) color("blue") cylinder(r=rodamiento_i_r, h=rodamiento_h, $fn=100);//aca - corr*3 a un externo  porque rodam es fijo. Luego lo saque porque bailaba
 }
 #color("red")translate([0,0,0])
         tap(type, turns=vueltas);
 }}   
   
 //-----------------------------------------------------3) main optical tube 
 

// translate([0,0,90]) tube_lens_V2();
module tube_lens_V2(){//focus control and lens holder all together
    difference() {     
        //external thread
            color("green") translate([0,0,0])
                pos_cylinder_thread(H=(ext_bot_len+extra_doublet*2+int_focal_corr_h-corr), type_of_thread="M33x0.75-ext");
        //color("green") english_thread (diameter=((tube_r+thread_thick)*2)/25.4, threads_per_inch=32, length=(ext_bot_len+extra_doublet*2+int_focal_corr_h-corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
        translate([ 0,0,-corr*2]) union(){     
//           //joint to ext_tube_V2
        tap("M27x0.75", turns=30); 
            len= (lens_holder_pos+extra_doublet+corr);
            echo ("lenght is",len);
//         translate([ 0.00, 0.00, -corr])   color ("lightblue") english_thread (diameter=(M27_r*2)/25.4, threads_per_inch=32, length=(lens_holder_pos+extra_doublet+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
    translate([ 0.00, 0.00, corr ]) color ("blue") cylinder(r= foc_adj_r+3*corr, h= ext_bot_len+extra_doublet*2+int_focal_corr_h-thread_h_cam_attach+2*corr);
            
//    joint to camera adapter
            translate([ 0.00, 0.00, ext_bot_len+extra_doublet*2+int_focal_corr_h-thread_h_cam_attach+2*corr ]) 
            
            color ("cyan")   //joint to ext_tube_V2
        tap("M18x0.75", turns=15); 
            
//            english_thread (diameter= (int_focal_corr_r*2)/25.4, threads_per_inch=32, length=(thread_h_cam_attach+2*corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
            echo(str ("total h is ",ext_bot_len+extra_doublet*2+int_focal_corr_h-corr)); 
            
            //notches to stop the whole thing from rotating freely
    reflect(1,0,0) color("blue") translate([tube_r+thread_thick/2+wall/2, 0, (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([ notch_x+corr*2, notch_y+corr*2, ext_bot_len+extra_doublet*2+int_focal_corr_h+corr ], center=true);       
   color("red") translate([0, tube_r+thread_thick/2+wall/2, (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([ notch_y+corr*2, notch_x+corr*2, ext_bot_len+extra_doublet*2+int_focal_corr_h+corr ], center=true);       
  color("red") translate([0, -(tube_r+thread_thick/2+wall/2), (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([ notch_y+corr*2, notch_x+corr*2, ext_bot_len+extra_doublet*2+int_focal_corr_h+corr ], center=true);       
    }}     }
   
    
 //----------------------------------------filter cube
   
    
//ext_tube_MM();//male male to join to filter cube
module ext_tube_MM(){//male male to join to filter cube
difference() {
    union(){ 
     cylinder(r=tube_r,h=MM_h, center=true,$fn=100);
     translate([0,0,-(MM_h/2+(c_mount_h)-2*corr)]) color("red") pos_cylinder_thread(c_mount_h, "M27x0.75-ext");
//        english_thread (diameter=((M27_r*2)-(3*corr))/25.4, threads_per_inch=32, length= c_mount_h/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
     translate([0,0,MM_h/2]) color("lightblue") pos_cylinder_thread(c_mount_h, "M27x0.75-ext");}
//        english_thread (diameter=((M27_r*2)-(3*corr))/25.4, threads_per_inch=32, length=c_mount_h/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);}
     
     translate ([0.00,0.00,-MM_h]) color("green") cylinder(r=RMS_r,h=MM_h*5+corr*2, center=true,$fn=100);
   }}
 
 
   
// translate([0,0,80]) rotate([0,180,0]) filter_cube_top();
module filter_cube_top(){
    translate([-(filter_cube_x+joint_x)/2,0,-joint_z/2]) joint();
    mirror([1,0,0]) translate([-(filter_cube_x+joint_x)/2,0,-joint_z/2]) joint();
    difference(){
    translate([ 0.00, 0.00, - filter_cube_top_z/2 ]) cube(size=[filter_cube_x,filter_cube_y, filter_cube_top_z], center= true);
    translate([ 0.00, 0.00, - filter_cube_top_z-corr])    color ("lightblue") tap("M27x0.75", 10);
//        english_thread (diameter=(M27_r*2)/25.4, threads_per_inch=32, length=lens_holder_pos/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1); 
    translate([ 0.00, 0.00, -filter_cube_top_z/2]) color ("red") cylinder(r=collimated_r,h= filter_cube_top_z+corr, center=true);
    }
    }  
    
// translate([0,0,80]) filter_cube_bott();
module filter_cube_bott(){
    echo(str ("collimated_r is ",collimated_r));
    translate([-(filter_cube_x+joint_x)/2,0,-joint_z/2]) joint();
    mirror([1,0,0]) translate([-(filter_cube_x+joint_x)/2,0,-joint_z/2]) joint();
    difference(){
    translate([ 0.00, 0.00, -filter_cube_z/2 ]) cube(size=[filter_cube_x,filter_cube_y,filter_cube_z], center= true);
    dic_house();
    translate([ 0.00, 0.00, -filter_cube_z])    color ("lightblue") pos_cylinder_thread(c_mount_h, "M27x0.75-ext");
//        english_thread (diameter=(M27_r*2)/25.4, threads_per_inch=32, length=c_mount_h/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1); 
    translate([ 0.00, 0.00, -filter_cube_z/2]) color ("green") cylinder(r=collimated_r,h=filter_cube_z+corr*10, center=true);
    translate([ 0.00, (filter_cube_y-y_triangle)/2+10, -y_triangle/2]) rotate([ 90.00, 0.00, 0.00 ]) color ("red") cylinder(r=collimated_r,h=filter_cube_x, center=true);
    }
    }  
 
//  translate([0,40,80 -y_triangle/2]) LED_in();
    module LED_in(){
            difference(){
                union(){
              rotate([ 90.00, 0.00, 0.00 ]) color ("red") cylinder(r=collimated_r,h=filter_cube_x/2, center=true);
            translate([ 0.00, filter_cube_y, 0 ])  cube(size=[filter_cube_x,filter_cube_y*2,filter_cube_x], center= true);
                    }
          translate([ 0.00, -filter_cube_x/4, 0 ])  cube(size=[2,filter_cube_x/2,filter_cube_x], center= true);   //notch in cylinder into filter box        
   union(){
          rotate([ 90.00, 0.00, 0.00 ]) color ("green") cylinder(r=collimated_r-wall/2,h=filter_cube_x+corr, center=true);
    translate([ 0.00,, 0 ])  rotate([ 90.00, 0.00, 0.00 ])  color ("green") cylinder(r=collimated_r-wall/2,h=filter_cube_x/2+corr, center=true);
               
         translate([0.00, filter_cube_y/2+ wall, 0 ]) color ("white") hull(){
                       rotate([ 90.00, 00.00, 0.00 ])  cylinder(r=collimated_r-wall/2,h=filter_cube_x/2+corr, center=true);//first ring width
       translate([ 0.00, filter_cube_y/2, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cylinder(r=ACL25416U_r+corr,h=filter_cube_y+corr, center=true);//second ring width for accommodating the aespherical lens
                   translate([ 0.00, filter_cube_y/2+BFL, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cylinder(r=M470L5_r+wall+corr,h=filter_cube_y+corr, center=true); //third width for accommodating the LED holder
       
        translate([ 0.00, filter_cube_y*2, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cylinder(r=M470L5_r+wall*1.5+corr,h=filter_cube_y+corr, center=true); //last ring width 
   
        
    }}
        // notch
         translate([ 0.00,filter_cube_x/2+filter_cube_y/2+ wall+ filter_cube_y/2+BFL+front_led, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cube([14,filter_cube_x,filter_cube_x*2], center= true);
 //back cut
      translate([ 0.00,filter_cube_x*2.7, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cube([120,filter_cube_x,filter_cube_x*2], center= true);
    }}
    
    

// translate([ 120.00, 40+filter_cube_y/2+ wall,80 -y_triangle/2 ]) M470L5_holder();
    module M470L5_holder(){
      difference(){
          union(){
                 // notch
        translate([ 0.00,filter_cube_x/2+ filter_cube_y/2+BFL+front_led, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cube([14-corr*2,filter_cube_x,filter_cube_x*2], center= true);
       color ("white") hull(){
//                       rotate([ 90.00, 00.00, 0.00 ])  cylinder(r=collimated_r-wall/2,h=filter_cube_x/2+corr, center=true);//first ring width
       translate([ 0.00, filter_cube_y/2, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cylinder(r=ACL25416U_r,h=filter_cube_y+corr, center=true);//second ring width for accommodating the aespherical lens
        translate([ 0.00, filter_cube_y/2+BFL, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cylinder(r=M470L5_r+wall,h=filter_cube_y+corr, center=true); //third width for accommodating the LED holder
       
        translate([ 0.00, filter_cube_y*2, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cylinder(r=M470L5_r+wall*1.5,h=filter_cube_y+corr, center=true); //last ring width 
    }}
    // internal
           union(){
                     // notch
        translate([ 0.00,filter_cube_x/2+ filter_cube_y/2+BFL+front_led, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cube([9,filter_cube_x,filter_cube_x*2], center= true);
              color ("white") hull(){
//                       rotate([ 90.00, 00.00, 0.00 ])  cylinder(r=collimated_r-wall/2,h=filter_cube_x/2+corr, center=true);//first ring width
       translate([ 0.00, filter_cube_y/2, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cylinder(r=ACL25416U_r-wall,h=filter_cube_y+corr, center=true);//second ring width for accommodating the aespherical lens
                   translate([ 0.00, filter_cube_y/2+BFL, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cylinder(r=M470L5_r+corr,h=filter_cube_y+corr, center=true); //third  width for accommodating the LED holder
        translate([ 0.00, filter_cube_y*2, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cylinder(r=M470L5_r+wall+corr,h=filter_cube_y+corr, center=true); //last ring width 
    }}
    
    //back cut
      translate([ 0.00,filter_cube_x*2.1, 0 ])   rotate([ 90.00, 00.00, 0.00 ]) color ("pink") cube([120,filter_cube_x,filter_cube_x*2], center= true);
}}

//---------------------------------------------extension tube to hold objective



//ext_tube_V2();//short extension used to join to the fluor cube or straight to optical tube holding the objetive at the bottom and the achromat lens inside the tube lens on the top.
module ext_tube_V2(){//Bottom part for fluorescent or polarizing filters
difference() {
    union(){ 
        translate([0,0,fluo_bottom_h/2]) color("lightblue") pos_cylinder_thread(3.5, "M27x0.75-ext");
//        english_thread (diameter=((M27_r*2)-(3*corr))/25.4, threads_per_inch=32, length=3.5/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
     cylinder(r=tube_r,h=fluo_bottom_h, center=true,$fn=100);}
     
    // this union is the complete internal thread, RMS for objective and anti-reflective internal surface
    translate ([0.00,0.00,-(fluo_bottom_h/2)]) color("red") tap("RMS", 10);// this creates an internal RMS thread for DIN (and JIS) objectives 20.1mm / 0.7965" dia. 36 TPI, 55° Whitworth
//        english_thread (diameter=((RMS_r*2)+corr*3)/25.4, threads_per_inch=36, length=(RMS_mount_h)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1)
     intlength=fluo_bottom_h *1.1;
       echo (" intlength ", intlength); //how many turns?
# translate ([ 0.00, 0.00,-(fluo_bottom_h/2) + RMS_mount_h])  color("green") 
        tap("M22x0.75",50);;// this creates an internal rough anti-reflective surface 
//        english_thread (diameter=(int_r*2)/25.4, threads_per_inch=32, length=(fluo_bottom_h *1.1)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal rough anti-reflective surface 
    }}
    

//---------------------------main body
  
// translate([ 0.00, 0.00,40]) cone_slide_fluor();
module cone_slide_fluor(){
 translate([ 0.00, 0.00,push_joint_h]){
   difference(){     
       //cone
   union(){
       color("pink")  hull(){
           translate([ 0.00, 0.00, cone_slide_h ]) 
//           pink cylinder(r=tube_r+thread_thick*4, h=1);
                                minkowski(){ 
                   cylinder(r=wall,h=1); 
                   cube(size=[filter_cube_x+wall*2,filter_cube_y+wall*2,filter_cube_z], center=true); }
 translate([0,0,-9.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=push_joint_h, center=true); 
                     cube([ push_joint_xy_V2, push_joint_xy_V2, 1 ], center=true);  };
//            minkowski(){ 
//                   cylinder(r=wall,h=1); 
//                    cube([ push_joint_xy_V2, push_joint_xy_adapt, 1.00 ], center=true);  }               
                     };
      
       //slide holder  
//                    color("blue") minkowski(){ 
//                 translate([ 0.00, 0.00, -1/2 ])   cylinder(r=wall,h=1, center=true);                 translate([ 0.00, 0.00, -1/2 ])      cube([ push_joint_xy_V2, push_joint_xy_V2, 1 ], center=true);}
 }
  translate([ 0, 0, -wall*4 ])  color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h+corr , center=true); 
                    cube([ push_joint_xy_V2-wall, push_joint_xy_V2-wall,  1 ], center=true); 
        }
        
        //window LED in       
          translate([ 0.00, 30, cone_slide_h/2 ]) cube([filter_cube_x+wall,filter_cube_x+wall,80], center=true);
          
       //internal                 
   color("white")  hull(){ 
       translate([ 0.00, 0.00, cone_slide_h ])
       minkowski(){ 
                   cylinder(r=wall,h=1); 
                                    cube(size=[filter_cube_x,filter_cube_y,filter_cube_z], center=true); }
                                      translate([ 0, 0, -wall*10 ])  color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h+corr , center=true); 
                    cube([ push_joint_xy_V2-2*wall, push_joint_xy_V2-2*wall,  1 ], center=true); 
        }
//       cylinder(r=tube_r+thread_thick+corr*6, h=1);
//            minkowski(){ 
//                   cylinder(r=wall,h=1); 
//                   translate([ 0.00, 0.00, 1/2 ])  cube([ push_joint_xy_V2-wall*3, push_joint_xy_V2*0.7-wall*3,  1.00 ], center=true);  }
                                    };
                            
//     arc window (better for printing without support)
                   translate([ 0.00, 0.00, slide_holder_z/2  -slide_x ]) color("red")   rotate([ 90, 0, 0 ]) cylinder( r=slide_x*1.2,h= slide_y+wall , center=true);     
       
                   }
                   
  translate([ 0.00, 0.00, htt_h+cone_slide_h]) difference(){
    translate([ 0, 0, 0 ]) color("blue")  cylinder(r=rodamiento_e_r+thread_thick, h=rodamiento_h,$fn=100);
    translate([0, 0, thread_thick]) color("pink")  cylinder(r=rodamiento_e_r+corr, h=rodamiento_h,$fn=100);
    translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);
    }                 
                    
translate([ 0.00, 0.00,cone_slide_h]) 
  union(){
      difference(){
        color("grey")  sequential_hull(){ 
            translate([ 0.00, 0.00, htt_h ]) cylinder(r=rodamiento_e_r+thread_thick,h=1); 
//            translate([ 0.00, 0.00, htt_h*0.7 ])   cylinder(r=rodamiento_e_r*0.8,h=1); 
//            cylinder(r=tube_r+thread_thick*4, h=1);   
            translate([ 0.00, 0.00, htt_h*0.5 ])    minkowski(){ 
                   cylinder(r=wall,h=1); 
                                    cube(size=[filter_cube_x+wall*2,filter_cube_y+wall*2,filter_cube_z], center=true); }     
                                    };
//          cylinder(r=tube_r+thread_thick*4, h=htt_h,$fn=100);
                translate([0, 0, -corr]) color("white")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*4,$fn=100);}
                
             //notches  //added corr /2 to blue and red to loosen it up a bit (25-11-20)
difference() {
   union(){ color("blue") translate([(tube_r+thread_thick/2+wall/2)+corr/2, 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);      
               color("green") translate([-(tube_r+thread_thick/2+wall/2+corr/2), 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);  
            color("red") translate([0, tube_r+thread_thick/2+wall/2+corr/2, htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);       
            color("cyan") translate([0, -(tube_r+thread_thick/2+wall/2+corr/2), htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);    }
           
            hull(){ 
       translate([ 0.00, 0.00, 0 ])
       minkowski(){ 
                   cylinder(r=wall,h=1); 
                                    cube(size=[filter_cube_x,filter_cube_y,filter_cube_z], center=true); }
                                      translate([ 0, 0, -wall ])  color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h+corr , center=true); 
                    cube([ push_joint_xy_V2-2*wall, push_joint_xy_V2-2*wall,  1 ], center=true); 
        }      };   }
                
translate([ 0.00, 0.00, htt_h]) difference(){
    translate([ 0, 0, 0 ]) color("blue")  cylinder(r=rodamiento_e_r+thread_thick, h=rodamiento_h,$fn=100);
    translate([0, 0, thread_thick]) color("pink")  cylinder(r=rodamiento_e_r+corr, h=rodamiento_h,$fn=100);
    translate([0, 0, -corr]) color("green")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);
    } }     }       }
       

//---------------------------------------------base

    

//      base_fluor();//for sample holding
module base_fluor(){
                difference(){   
                    //Outer      
                    union(){
                   //top joint
             translate([ 0,0, push_joint_h/2-wall ])  color("grey")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ push_joint_xy_V2-wall-corr*2, push_joint_xy_V2-wall-corr*2,  1 ], center=true);};
                    // base
             translate([ 0.00, 0.00, -base_h*2])    minkowski(){ 
                    cylinder(r=wall,h=base_h, center=true); 
                    cube([ push_joint_xy_V2, push_joint_xy_V2,  base_h*4 ], center=true);
                         };
                                   // stage
                   difference(){  translate([ 0.00, 0.00, -base_h*6-1])   minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=base_h*4 , center=true); 
                    cube([ push_joint_xy_V2, push_joint_xy_V2,  1 ], center=true);};
                       //internal part bottom base
                     translate([ 0.00, 0.00, -base_h*6-1])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy_V2-wall*4, push_joint_xy_V2-wall*4,  base_h*4+push_joint_h ], center=true);   };
                         
                }
                          
                          difference(){
                                    // base piso
                     translate([ 0.00, 0.00, -base_h*8-1])    minkowski(){ 
                    cylinder(r=wall,h=2, center=true); 
                    cube([ push_joint_xy_V2*1.4, push_joint_xy_V2*1.4,  2 ], center=true);
                          };
                              translate([ 0.00, 0.00, -base_h*8-1])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy_V2-wall*4, push_joint_xy_V2-wall*4,  base_h*4+push_joint_h ], center=true);  };
                    //internal part bottom base
                     translate([ 0.00, 0.00, -base_h*6-1])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy_V2-wall*4, push_joint_xy_V2-wall*4,  base_h*4+push_joint_h ], center=true);   };
                          }
          }
            //internal part     
              translate([ 0.00, 0.00, -base_h*2+wall*4])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy_V2-wall*4, push_joint_xy_V2-wall*4,  base_h*4+push_joint_h ], center=true);
                         };
               translate([ 0.00, 0.00, -push_joint_h/2-wall  ]) 
                    cube([ push_joint_xy_V2*0.8, push_joint_xy_V2*2,  base_h*4 ], center=true);
                         //     arc window (better for printing without support)
                difference(){   translate([ 0.00, 0.00, push_joint_h/2-wall  ]) color("red")   rotate([ 90, 0, 0 ]) cylinder( r=slide_x*1.2,h= slide_y+wall , center=true);     
                     translate([ 0.00, 0.00, -push_joint_h/2-wall-excess_obj  ]) minkowski(){ 
                    cylinder(r=wall,h=base_h, center=true); 
                    cube([ push_joint_xy_V2, push_joint_xy_V2,  base_h*4 ], center=true);
                         };                       
                }
                 
                   //light internal path
                 minkowski(){ 
                cylinder(r=wall,h=1); 
              translate([ 0.00, 0.00, -base_h*4-1 ])  cube([ push_joint_xy_V2*0.5, push_joint_xy_V2*0.5-wall*3,  100.00 ], center=true);  }; 
                  //cable
                  #translate([  push_joint_xy_V2/2,0.00,-base_h*8+wall*2])  rotate([ 90, 0, 90 ])cylinder(r=4,h=5, center=true);      
                      }      
            }
    


//     base_ancha();//last used (short)
module base_ancha(){
                difference(){   
                    //Outer      
                    union(){
                   //top joint
             translate([ 0,0, push_joint_h/2-wall ])  color("grey")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ push_joint_xy_V2-wall-corr*2, push_joint_xy_V2-wall-corr*2,  1 ], center=true);};
                    // base
             translate([ 0.00, 0.00, -base_h*2])    minkowski(){ 
                    cylinder(r=wall,h=base_h, center=true); 
                    cube([ push_joint_xy_V2, push_joint_xy_V2,  base_h*4 ], center=true);
                         };
                    
                  //base joint
//              translate([ 0.00, 0.00, -push_joint_h])    hull(){ minkowski(){ 
//                    cylinder(r=wall,h=push_joint_h , center=true); 
//                    cube([ push_joint_xy, push_joint_xy,  1 ], center=true);
//                         };}
                        // base piso
                     translate([ 0.00, 0.00, -base_h*4-1])    minkowski(){ 
                    cylinder(r=wall,h=2, center=true); 
                    cube([ push_joint_xy_V2*1.4, push_joint_xy_V2*1.4,  2 ], center=true);
                          
            }}
            //internal part
            
              translate([ 0.00, 0.00, -base_h*2+wall*4])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy_V2-wall*4, push_joint_xy_V2-wall*4,  base_h*4+push_joint_h ], center=true);
                         };
              //cable
                  translate([ 0.00, push_joint_xy_V2/2,-base_h*4+wall*2])  rotate([ 90, 0, 0 ])cylinder(r=4,h=5, center=true);       
            }}
            
            
// led_holder();
 module led_holder(condenser=true){
      
          difference(){   
             //internal part
           union(){
               if (condenser==true){
         translate([0, 0, base_h_led_holder-wall*2])  tall_condenser();
          }
               minkowski(){ 
                    cylinder(r=wall,h=base_h, center=true); 
                    cube([ push_joint_xy_V2-wall*4-corr*2, push_joint_xy_V2-wall*4-corr*2,  base_h_led_holder ], center=true);
           };

                     }
                         //internal
                          translate([ 0.00, 0.00,-wall*2])    minkowski(){ 
                    cylinder(r=wall,h=base_h, center=true); 
                    cube([ push_joint_xy_V2-wall*6, push_joint_xy_V2-wall*6,  base_h_led_holder ], center=true);
                         };
                         //LED hole
                         cylinder(r=5/2+corr,h=1000);
                color("white") translate([ 0.00, push_joint_xy_V2/2-wall*2, -base_h_led_holder/1.8])  rotate([ 90, 0, 0 ])cylinder(r=3,h=5, center=true);
                        //handle 
                      translate([ -push_joint_xy_V2/2, 0.00,0])       cylinder(r= 25/2, h=push_joint_xy_V2, center=true);
                           translate([ push_joint_xy_V2/2, 0.00,0])       cylinder(r= 25/2, h=push_joint_xy_V2, center=true);
            }}
            

 //-----------------------------------------polarizer module
          
          

//translate ([ 0.00, 0.00, -35.00 ]) pol_ring();
module pol_ring(){
    difference() {
        union(){
        translate([0,0,-(0)]) color("green") cylinder(r=pol_hold_r-wall*2-corr,h=cyl_h,$fn=100);
        translate([pol_r,-holder_y/2,0]) color("yellow") cube([holder_x*4,holder_y*3,holder_z]);
            }
       union(){ 
        translate([0,0,-(pol_h+corr)]) color("white") cylinder(r=pol_r-1,h=(pol_h*2)+corr,$fn=100);
   translate([0,0,pol_h]) color("red")  pos_cylinder_thread(pol_h*6, "M27x0.75-ext");
//           english_thread (diameter=(M27_r*2+corr)/25.4, threads_per_inch=32, length=pol_h*6/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);}
    }    
    }}
   
  
                
                
//pol_ext_V4();//last used
module pol_ext_V4(){      difference(){   
                    //Outer      
                    union(){
                   //top joint
             translate([ 0,0, -push_joint_h/2-wall ])  color("grey")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ push_joint_xy_V2-wall-corr*2, push_joint_xy_V2-wall-corr*2,  1 ], center=true);};
                    // dent
              translate([ 0.00, 0.00, - push_joint_h-dent_h/2])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy_V2, push_joint_xy_V2,  1 ], center=true);
                         };
                     //decr hips    
              translate([ 0.00, 0.00, - push_joint_h -dent_h ])hull(){   
                     minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy_V2, push_joint_xy_V2,  1 ], center=true);
                         };
                    translate([ 0.00, 0.00, -decr_h])  cylinder(r=pol_hold_r+thick,h=1, $fn=100); 
                    }
                     //pol ring control
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h/2])cylinder(r=pol_hold_r+thick,h=lens_hold_h, center=true,$fn=100);            
                    
                    //incr hips    
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h ]) hull(){   
                   cylinder(r=pol_hold_r+thick,h=1, $fn=100); 
                    translate([ 0.00, 0.00, -decr_h*3])    minkowski(){ 
                    cylinder(r=wall,h=push_joint_h, center=true); 
                     cube([ push_joint_xy_V2, push_joint_xy_V2, 1 ], center=true);  };
               }
                  //base joint
              translate([ 0.00, 0.00,, -push_joint_h-dent_h -decr_h-lens_hold_h-decr_h-push_joint_h/2])    translate([0,0,-0.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=push_joint_h, center=true); 
                     cube([ push_joint_xy_V2, push_joint_xy_V2, 1 ], center=true);  };
            }
            
            //internal part
               union(){
                   //top joint
              translate([ 0,0, -push_joint_h/2+corr ])  color("grey")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h, , center=true); 
                    cube([ push_joint_xy_V2-wall*3-corr*2, push_joint_xy_V2-wall*3-corr*2,  1 ], center=true);};
                    // dent
              translate([ 0.00, 0.00, - push_joint_h-dent_h/2])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy-wall*3, push_joint_xy-wall*3,  1 ], center=true);
                         };
                     //decr hips    
              translate([ 0.00, 0.00, - push_joint_h -dent_h ]) color("white") hull(){   
                     minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy_V2-wall*4, push_joint_xy_V2-wall*4,  1 ], center=true);
                         };
                    translate([ 0.00, 0.00, -decr_h])  cylinder(r=pol_hold_r+thick-wall*2,h=1, $fn=100); 
                    }
            //pol ring control
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h/2]) 
                    union(){
                   translate([ 0, 0, 10 ])color("lightgreen") cylinder(r=pol_hold_r+corr-wall*2,h=lens_hold_h+20, center=true,$fn=100);   
                  translate([-cond_lens_r,0, -lens_hold_h/2])rotate([0, 0, -45]) color("pink") cube([holder_x*20,holder_x*20,holder_z+corr*6]);// slit for pol ring slider
              union(){  // rotational space for pol ring 
            translate([cond_lens_r-thick*3, 0.00, - lens_hold_h/2]) color("blue") cube([holder_x*10,holder_y+(corr*2),holder_z*20]); // holder entrance slit
              }
                  }         
                    //incr hips    
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h ]) color("blue") hull(){   
                   cylinder(r=pol_hold_r+thick-wall*5,h=1, $fn=100); 
                    translate([ 0.00, 0.00, -decr_h])   minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    translate([ 0.00, 0.00, -decr_h*6]) cube([ push_joint_xy_V2-wall, push_joint_xy_V2-wall,  1 ], center=true); 
                         };
               }
                  //base joint
              translate([ 0.00, 0.00,, -push_joint_h-dent_h -decr_h-lens_hold_h-decr_h-push_joint_h/2-wall])    minkowski(){ 
                    cylinder(r=wall,h=push_joint_h+corr , center=true); 
                    cube([ push_joint_xy_V2-wall, push_joint_xy_V2-wall,  1 ], center=true); 
        }
        
            }
                }
                }    
            
     


//------------------------------------------ring holders and tools for screwing/unscrewing it

    
//rotate(90,0,0) lens_holder_ring_V2();
module lens_holder_ring_V2(){//internal lens retainer ring mas chico
tot_len=2.5;
difference() {
    color("red") pos_cylinder_thread(tot_len, "M27x0.75-ext");                   

//    english_thread (diameter=(((M27_r*2)-(corr*5))/25.4), threads_per_inch=32, length=tot_len/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal RMS thread for DIN objectives
    translate([ 0.00, 0.00, -corr*2 ])cylinder(r=int_r-corr*2,h=tot_len*2+corr*2);
    cube([ 1, 100, 2 ], center=true);
}}      


    
// lens_holder_ring_tool();
module lens_holder_ring_tool(){//to rotate ring
tot_len=2.5;
difference() {
   hull(){
        translate([ 0.00, 0.00, 0 ])  cube([(M27_r*2)-(corr*8), 1, 1], center=true) ;
    translate([ 0.00, 0.00, M27_r ])   cylinder(r=(M27_r/2)-(corr*10), h=1, center=true); 
    }
    translate([ 0.00, 0.00, -M27_r/2 ]) sphere(r=(M27_r)-(corr*3));
}
 translate([ 0.00, 0.00, M27_r*2 ])   cylinder(r=(M27_r/2)-(corr*10), h=M27_r*2, center=true); 
}      




//cone_slide(); //not fluor version
module cone_slide(){
 {translate([ 0.00, 0.00,push_joint_h]){
   difference(){     
       //cone
   union(){
       color("pink")  hull(){ translate([ 0.00, 0.00, cone_slide_h ]) 
            cylinder(r=tube_r+thread_thick*4, h=1);
            minkowski(){ 
                    cylinder(r=wall,h=1); 
                    cube([ push_joint_xy_V2, push_joint_xy_adapt, 1.00 ], center=true);  }};
      
       //slide holder  
                    color("blue") minkowski(){ 
                  translate([ 0.00, 0.00, -1/2 ])   cylinder(r=wall,h=1, center=true); 
                translate([ 0.00, 0.00, -1/2 ])      cube([ push_joint_xy_V2, push_joint_xy_V2, 1 ], center=true);}
 }
 
       //internal             
   color("yellow")   hull(){ 
       translate([ 0.00, 0.00, cone_slide_h ]) cylinder(r=tube_r+thread_thick+corr*6, h=1);
            minkowski(){ 
                   cylinder(r=wall,h=1); 
                   translate([ 0.00, 0.00, 1/2 ])  cube([ push_joint_xy_V2-wall*3, push_joint_xy_V2*0.7-wall*3,  1.00 ], center=true);  }};
          
       //slide holder operation space        
       translate([ 0.00, 0.00, slide_holder_z/2 ]) //the minus 1 is because of the floor of internal cone hull
                            
//     arc window (better for printing wo support)
                   translate([ 0.00, 0.00, -slide_x/2 ]) color("red")   rotate([ 90, 0, 0 ]) cylinder( r=slide_x,h= slide_y+wall , center=true);     
             //square window      
//           translate([ 0.00, 0.00, 0 ]) color("red")   cube([ slide_x*1.85, slide_y+wall, slide_holder_z ], center=true);          
                         //light internal path
                   minkowski(){ 
                   cylinder(r=wall,h=1); 
                   translate([ 0.00, 0.00, 1/2 ])  cube([ push_joint_xy_V2*0.5, push_joint_xy_V2*0.5-wall*3,  10.00 ], center=true);  };
                   }
                    
translate([ 0.00, 0.00,cone_slide_h]) 
  union(){
      difference(){
        color("grey")  sequential_hull(){ 
            translate([ 0.00, 0.00, htt_h ])   cylinder(r=rodamiento_e_r+thread_thick,h=1); 
            translate([ 0.00, 0.00, htt_h*0.7 ])   cylinder(r=rodamiento_e_r*0.8,h=1); 
            cylinder(r=tube_r+thread_thick*4, h=1);   };
                    
//          cylinder(r=tube_r+thread_thick*4, h=htt_h,$fn=100);
                translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*4,$fn=100);}
     //notches  //added corr /2 to blue and red to loosen it up a bit (25-11-20)
color("blue") translate([(tube_r+thread_thick/2+wall/2)+corr/2, 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);      
               color("green") translate([-(tube_r+thread_thick/2+wall/2+corr/2), 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);  
            color("red") translate([0, tube_r+thread_thick/2+wall/2+corr/2, htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);       
            color("cyan") translate([0, -(tube_r+thread_thick/2+wall/2+corr/2), htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);    
                
   
translate([ 0.00, 0.00, htt_h]) difference(){
    translate([ 0, 0, 0 ]) color("blue")  cylinder(r=rodamiento_e_r+thread_thick, h=rodamiento_h,$fn=100);
    translate([0, 0, thread_thick]) color("pink")  cylinder(r=rodamiento_e_r+corr, h=rodamiento_h,$fn=100);
    translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);
    }
}
        }   
//push_joint between cone and illumination base
  translate([ 0.00, 0.00,push_joint_h/2]){
      difference(){
    translate([0,0,-0.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=push_joint_h, center=true); 
                     cube([ push_joint_xy_V2, push_joint_xy_V2, 1 ], center=true);  };
   translate([ 0, 0, -wall ])  color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h+corr , center=true); 
                    cube([ push_joint_xy_V2-wall, push_joint_xy_V2-wall,  1 ], center=true); 
        }
                    
                    //light internal path
                   translate([ 0, 0, push_joint_h/2  ])  minkowski(){ 
                   cylinder(r=wall,h=1); 
                   translate([ 0.00, 0.00, 1/2 ])  cube([ push_joint_xy_V2*0.5, push_joint_xy_V2*0.5-wall*3,  10.00 ], center=true);  };
                    }}
        }

}

    
    /////////------------old modules

        
            
            

//base_ancha();
//COP_focus_lens_tubeV4_thrlong(); /*internal sin corr y external con 3*corr*/
//COP_focus_lens_tubeV3();// if using normal planoconvex thorlabs lens
//lens_holder_ring();//to keep lens fixed in position 
     //options:
        //ext_tube(); 
        //ext_fluo_top();
        //ruedita_roda(); //it is the focus-adjusting knob (corr*3 is the best fit in diam (total eh) corr*4 en Sep2020 para evitar saltos or corr*5 para Ender, it could be 5corr for loose turning in Zortrax too)
            
            
            
//focus_lens_thrlong_ocular(); 
    module focus_lens_thrlong_ocular(){
    difference() {

        color("green") english_thread (diameter=((tube_r+thread_thick)*2)/25.4, threads_per_inch=32, length=(ext_bot_len+extra_doublet*2+int_focal_corr_h-corr+insert_h)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
        
        translate([ 0,0,-corr]) union(){ 
         translate([ 0.00, 0.00, -corr])   color ("pink") english_thread (diameter=(M27_r*2)/25.4, threads_per_inch=32, length=(lens_holder_pos+extra_doublet+corr+insert_h)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
    translate([ 0.00, 0.00, corr ]) color ("red") cylinder(r= foc_adj_r+3*corr, h= ext_bot_len+extra_doublet*2+int_focal_corr_h-thread_h_cam_attach+2*corr+insert_h);
    translate([ 0.00, 0.00, ext_bot_len+extra_doublet*2+int_focal_corr_h-thread_h_cam_attach+2*corr+insert_h ]) color ("cyan") english_thread (diameter= (int_focal_corr_r*2)/25.4, threads_per_inch=32, length=(thread_h_cam_attach+2*corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
            echo(str ("total h is ",ext_bot_len+extra_doublet*2+int_focal_corr_h-corr)); 
           
            //notches to stop the whole thing from rotating freely (le saque un corr a cada uno el 03_03_2020)
            
    reflect(1,0,0) color("blue") translate([tube_r+thread_thick/2+wall/2, 0, (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([ notch_x+corr, notch_y+corr, ext_bot_len+extra_doublet*100 ], center=true);       
   color("red") translate([0, tube_r+thread_thick/2+wall/2, (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([ notch_y+corr, notch_x+corr, ext_bot_len+extra_doublet*100 ], center=true);       
  color("red") translate([0, -(tube_r+thread_thick/2+wall/2), (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([ notch_y+corr, notch_x+corr, ext_bot_len+extra_doublet*100], center=true);       
    }
    translate ([0.00,0.00,ext_bot_len+extra_doublet*2+int_focal_corr_h-corr+insert_h-ocular_h/2]) color("yellow") cylinder(r=ocular_r+corr,h=ocular_h,$fn=100, center=true);
    }}
 
    //lens_holder_pos+extra_doublet+corr+insert_h
 
//ocular_cam_adapter();
module ocular_cam_adapter(){
    difference() {
    union(){ 
        translate([0,0,ocular_cam_h+ocular_in_h/2]) color("lightblue") cylinder(r=ocular_r,h=ocular_in_h,$fn=100, center=true); //el que acepta es cylinder(r=ocular_r+corr,h=ocular_h,$fn=100, center=true);
        
//        translate([ 0,0, int_focal_corr_h/2 ]) cube([tube_r*2,tube_r*2, int_focal_corr_h],center=true,$fn=100);
   color("pink") cylinder(r=tube_r,h=ocular_cam_h,$fn=100);
        }
    translate ([0.00,0.00,-(corr)]) color("red") english_thread (diameter=(int_focal_corr_r*2)/25.4, threads_per_inch=32, length=((tot_len_focus_adjust_tube)+(ocular_h*corr))/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
//         translate ([0.00,0.00,-(corr)]) color("green") cylinder(r=ocular_r+corr,h=ocular_cam_h-wall,$fn=100);
     }    
    }
//ocular(); //es para adaptar al modelo que aceptaba focus adjust y camaras
module ocular(){
    difference() {
    union(){ 
        translate([0,0,ocular_h]) color("lightblue") english_thread (diameter=(foc_adj_r*2)/25.4, threads_per_inch=32, length=16/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
//        translate([ 0,0, int_focal_corr_h/2 ]) cube([tube_r*2,tube_r*2, int_focal_corr_h],center=true,$fn=100);
   color("pink") cylinder(r=tube_r,h=ocular_h,$fn=100);
        }
    translate ([0.00,0.00,-(corr)]) color("red") english_thread (diameter=(int_focal_corr_r*2)/25.4, threads_per_inch=32, length=((tot_len_focus_adjust_tube)+(ocular_h*corr))/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
         translate ([0.00,0.00,-(corr)]) color("green") cylinder(r=ocular_r+corr,h=ocular_h-wall,$fn=100);
     }    
    }

//cone_hand_scope_shortM12();    //por ahora  corr/2 en red and blue y los otros dos no
module cone_hand_scope_shortM12(){
 {translate([ 0.00, 0.00,push_joint_h]){
   difference(){     
       //cone
   union(){
       color("white")  hull(){ translate([ 0.00, 0.00, cone_slide_h + push_joint_h ]) 
            cylinder(r=tube_r+thread_thick*4, h=1);
            translate([0,0,50])minkowski(){ //aca acorte todo, en el hull
                    cylinder(r=wall,h=1); 
                    cube([ push_joint_xy_V2*0.75, push_joint_xy_V2*0.75, 1.00 ], center=true);  }};
 }
//       //internal             
//   color("yellow")   hull(){ 
//       translate([ 0.00, 0.00, cone_slide_h ]) cylinder(r=tube_r+thread_thick+corr*6, h=1);
//            translate([0,0,50]) minkowski(){ //aca acorte en el hull con 50mm
//                   cylinder(r=wall,h=1); 
//                   translate([ 0.00, 0.00,0 ])  cube([ push_joint_xy_V2*0.75-wall*3, push_joint_xy_V2*0.75-wall*3,  1.00 ], center=true);  }};
          
                   //ventanas de patitas
                   rotate([0,0,45]) color("orange")   hull(){ 
       translate([ 0.00, 0.00, cone_slide_h ]) cylinder(r=tube_r+thread_thick+corr*6, h=1);
            translate([0,0,50]) minkowski(){ 
                   cylinder(r=wall,h=1); 
                   translate([ 0.00, 0.00,0 ])  cube([ push_joint_xy_V2*0.95-wall*3, push_joint_xy_V2*0.95-wall*3,  1.00 ], center=true);  }};
          
                   //internal cyl hole       
 translate([0, 0, -corr-htt_h]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*4,$fn=100);
                   
                   //LEDs holes
                 #translate([ 0.00, 0.00,cone_slide_h/2+push_joint_h-5])  rotate([ 70, 0, 45 ]) cylinder(r=5/2+corr,h=40);   
                    #mirror()translate([ 0.00, 0.00,cone_slide_h/2+push_joint_h-5])  rotate([ 70, 0, 45 ]) cylinder(r=5/2+corr,h=40);   
                   
                   }   
                   
                   //top part
translate([ 0.00, 0.00,cone_slide_h]) 
  union(){
      difference(){
        color("grey")  sequential_hull(){ 
            translate([ 0.00, 0.00, htt_h ])   cylinder(r=rodamiento_e_r+thread_thick,h=1); 
            translate([ 0.00, 0.00, htt_h*0.7 ])   cylinder(r=rodamiento_e_r*0.8,h=1,$fn=60); 
            cylinder(r=tube_r+thread_thick*4, h=1,$fn=60);   };
                    
//          cylinder(r=tube_r+thread_thick*4, h=htt_h,$fn=100);
                translate([0, 0, -corr-htt_h]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*4,$fn=100);}
 
    //notches  //added corr /2 to blue and red to loosen it up a bit (25-11-20)
color("blue") translate([(tube_r+thread_thick/2+wall/2)+corr/2, 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);      
               color("green") translate([-(tube_r+thread_thick/2+wall/2), 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);  
            color("red") translate([0, tube_r+thread_thick/2+wall/2+corr/2, htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);       
            color("cyan") translate([0, -(tube_r+thread_thick/2+wall/2), htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);     
 
                //top bearing holder
translate([ 0.00, 0.00, htt_h]) difference(){
    translate([ 0, 0, 0 ]) color("blue")  cylinder(r=rodamiento_e_r+thread_thick, h=rodamiento_h,$fn=100);
    translate([0, 0, thread_thick]) color("pink")  cylinder(r=rodamiento_e_r+corr, h=rodamiento_h,$fn=100);
    translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);
    }
}
        }   
}
        }
 


                
//translate([ 0, 0, -50 ]) pol_ext_V3();//last used
module pol_ext_V3(){
                difference(){   
                    //Outer      
                    union(){
                   //top joint
             translate([ 0,0, -push_joint_h/2-wall ])  color("grey")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ push_joint_xy_V2-wall-corr*2, push_joint_xy_V2-wall-corr*2,  1 ], center=true);};
                    // dent
              translate([ 0.00, 0.00, - push_joint_h-dent_h/2])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy_V2, push_joint_xy_V2,  1 ], center=true);
                         };
                     //decr hips    
              translate([ 0.00, 0.00, - push_joint_h -dent_h ])hull(){   
                     minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy_V2, push_joint_xy_V2,  1 ], center=true);
                         };
                    translate([ 0.00, 0.00, -decr_h])  cylinder(r=pol_hold_r+thick,h=1, $fn=100); 
                    }
                     //pol ring control
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h/2])cylinder(r=pol_hold_r+thick,h=lens_hold_h, center=true,$fn=100);            //incr hips    
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h ]) hull(){   
                   cylinder(r=pol_hold_r+thick,h=1, $fn=100); 
                    translate([ 0.00, 0.00, -decr_h*3])   minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy, push_joint_xy,  1 ], center=true);
                         };
               }
                  //base joint
              translate([ 0.00, 0.00,, -push_joint_h-dent_h -decr_h-lens_hold_h-decr_h-push_joint_h/2])    hull(){ minkowski(){ 
                    cylinder(r=wall,h=push_joint_h , center=true); 
                    cube([ push_joint_xy, push_joint_xy,  1 ], center=true);
                         };
                     }
            }
            //internal part
               union(){
                   //top joint
              translate([ 0,0, -push_joint_h/2+corr ])  color("grey")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h, , center=true); 
                    cube([ push_joint_xy_V2-wall*3-corr*2, push_joint_xy_V2-wall*3-corr*2,  1 ], center=true);};
                    // dent
              translate([ 0.00, 0.00, - push_joint_h-dent_h/2])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy-wall*3, push_joint_xy-wall*3,  1 ], center=true);
                         };
                     //decr hips    
              translate([ 0.00, 0.00, - push_joint_h -dent_h ])hull(){   
                     minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy_V2-wall*4, push_joint_xy_V2-wall*4,  1 ], center=true);
                         };
                    translate([ 0.00, 0.00, -decr_h])  cylinder(r=pol_hold_r+thick-wall*2,h=1, $fn=100); 
                    }
            //pol ring control
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h/2]) 
                    union(){
                   translate([ 0, 0, 10 ])color("lightgreen") cylinder(r=pol_hold_r+corr,h=lens_hold_h+20, center=true,$fn=100);   
                 translate([0,-cond_lens_r, -lens_hold_h/2]) color("pink") cube([holder_x*5,holder_y*20,holder_z+corr*5]);// slit for pol ring slider
              union(){  // rotational space for pol ring 
            translate([cond_lens_r-thick*3, 0.00, - lens_hold_h/2]) color("blue") cube([holder_x*10,holder_y+(corr*2),holder_z*20]); // holder entrance slit
              }
                  }         
                    //incr hips    
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h ]) hull(){   
                   cylinder(r=pol_hold_r+thick-wall*2,h=1, $fn=100); 
                    translate([ 0.00, 0.00, -decr_h])   minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy-wall*3, push_joint_xy-wall*3,  1 ], center=true);
                         };
               }
                  //base joint
              translate([ 0.00, 0.00,, -push_joint_h-dent_h -decr_h-lens_hold_h-decr_h-push_joint_h/2])    hull(){ minkowski(){ 
                    cylinder(r=wall,h=push_joint_h , center=true); 
                    cube([ push_joint_xy-wall, push_joint_xy-wall,  1 ], center=true);
                         };
                         
                     }
            }
                }
                }
                
// translate([ 0, 0, -50 ]) pol_ext_V2();
module pol_ext_V2(){
                difference(){   
                    //Outer      
                    union(){
                   //top joint
              translate([ 0,0, -push_joint_h/2 ])  color("grey")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h, , center=true); 
                    cube([ push_joint_xy-wall-corr*2, push_joint_xy-wall-corr*2,  1 ], center=true);};
                    // dent
              translate([ 0.00, 0.00, - push_joint_h-dent_h/2])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy, push_joint_xy,  1 ], center=true);
                         };
                     //decr hips    
              translate([ 0.00, 0.00, - push_joint_h -dent_h ])hull(){   
                     minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy, push_joint_xy,  1 ], center=true);
                         };
                    translate([ 0.00, 0.00, -decr_h])  cylinder(r=pol_hold_r+thick,h=1, $fn=100); 
                    }
                     //pol ring control
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h/2])cylinder(r=pol_hold_r+thick,h=lens_hold_h, center=true,$fn=100);            //incr hips    
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h ]) hull(){   
                   cylinder(r=pol_hold_r+thick,h=1, $fn=100); 
                    translate([ 0.00, 0.00, -decr_h])   minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy, push_joint_xy,  1 ], center=true);
                         };
               }
                  //base joint
              translate([ 0.00, 0.00,, -push_joint_h-dent_h -decr_h-lens_hold_h-decr_h-push_joint_h/2])    hull(){ minkowski(){ 
                    cylinder(r=wall,h=push_joint_h , center=true); 
                    cube([ push_joint_xy, push_joint_xy,  1 ], center=true);
                         };
                     }
            }
            //internal part
               union(){
                   //top joint
              translate([ 0,0, -push_joint_h/2 ])  color("grey")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h, , center=true); 
                    cube([ push_joint_xy-wall*3-corr*2, push_joint_xy-wall*3-corr*2,  1 ], center=true);};
                    // dent
              translate([ 0.00, 0.00, - push_joint_h-dent_h/2])    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_xy-wall*3, push_joint_xy-wall*3,  1 ], center=true);
                         };
                     //decr hips    
              translate([ 0.00, 0.00, - push_joint_h -dent_h ])hull(){   
                     minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy-wall*4, push_joint_xy-wall*4,  1 ], center=true);
                         };
                    translate([ 0.00, 0.00, -decr_h])  cylinder(r=pol_hold_r+thick-wall*2,h=1, $fn=100); 
                    }
            //pol ring control
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h/2]) 
                    union(){
                   color("lightgreen") cylinder(r=pol_hold_r+corr,h=lens_hold_h+thick*2, center=true,$fn=100);   
                 translate([0,-cond_lens_r, -lens_hold_h/2]) color("pink") cube([holder_x*5,holder_y*20,holder_z+corr*2]);// slit for pol ring slider
              union(){  // rotational space for pol ring 
            translate([cond_lens_r-thick*3, 0.00, - lens_hold_h/2]) color("blue") cube([holder_x,holder_y+(corr*2),holder_z*8]); // holder entrance slit
              }
                  }         
                    //incr hips    
               translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-lens_hold_h ]) hull(){   
                   cylinder(r=pol_hold_r+thick-wall*2,h=1, $fn=100); 
                    translate([ 0.00, 0.00, -decr_h])   minkowski(){ 
                    cylinder(r=wall,h=1 , center=true); 
                    cube([ push_joint_xy-wall*3, push_joint_xy-wall*3,  1 ], center=true);
                         };
               }
                  //base joint
              translate([ 0.00, 0.00,, -push_joint_h-dent_h -decr_h-lens_hold_h-decr_h-push_joint_h/2])    hull(){ minkowski(){ 
                    cylinder(r=wall,h=push_joint_h , center=true); 
                    cube([ push_joint_xy-wall, push_joint_xy-wall,  1 ], center=true);
                         };
                         
                     }
            }
                }
                //dent for polarizer to stay in place
                 translate([ 0.00, 0.00, -push_joint_h-dent_h -decr_h-dent_h-lens_hold_h/2  -lens_hold_h/2]) difference(){
                     cylinder(r=pol_hold_r+wall*1.5,h=dent_h, center=true,$fn=100);  
                     cylinder(r=pol_hold_r-wall/2,h=dent_h, center=true,$fn=100);  
                     }
                
                }
// translate([ 0, 0, 5 ]) pol_holder();
module pol_holder(){
    difference(){
        union(){
        translate([ 0.00, 0.00, lens_hold_h ]) color("white")cylinder(r1=cond_lens_r+thick,r2=pol_hold_r+thick, h=cond_h,$fn=100);
      
        translate([ 0.00, 0.00, lens_hold_h + cond_h]) color("lightgreen") cylinder(r=pol_hold_r+thick,h=lens_hold_h, $fn=100);} //External
        union(){ 
               translate([0,-cond_lens_r, lens_hold_h + cond_h -thick]) color("pink") cube([holder_x*5,holder_y*20,holder_z+corr*2]);// slit for pol ring slider
            translate([ 0.00, 0.00, lens_hold_h + cond_h -thick]) color("red") cylinder(r=pol_hold_r+corr,h=lens_hold_h+thick*2, $fn=100);
              union(){  // rotational space for pol ring 
            translate([cond_lens_r-thick*3, 0.00, lens_hold_h + cond_h -thick]) color("blue") cube([holder_x,holder_y+(corr*2),holder_z*8]); // holder entrance slit
            //internal
          
            }}
  }
}

//translate([ 0, 0, -50 ])    lighting_base();              

module lighting_base(){
       difference(){  
            translate([ 0,0, -lighting_base_h/2 ]) color("white") minkowski(){ 
                    cylinder(r=wall,h=lighting_base_h, center=true); 
                     cube([ push_joint_xy, push_joint_xy, 1 ], center=true);  };   
            translate([ 0,0, -push_joint_h/2+wall ]) // shorter to not crash the slide
                     difference(){
                color("green") minkowski(){ 
                    cylinder(r=wall,h=push_joint_h, center=true); 
                     cube([ push_joint_xy, push_joint_xy, 1 ], center=true);  };
                    color("grey")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h,  center=true); 
                    cube([ push_joint_xy-wall-corr*2, push_joint_xy-wall-corr*2,  1 ], center=true);  }
                    };
                    translate([ 0,0, -lighting_base_h/2+wall ]) color("blue") minkowski(){ 
                    cylinder(r=wall,h=lighting_base_h, center=true); 
                    cube([ push_joint_xy-wall*2, push_joint_xy-wall*2, 1 ], center=true);  };
                    translate([ 0,push_joint_xy/2, -lighting_base_h+wall ]) rotate([90,0,0]) {cylinder(r=3,h=30, center=true);};   
                }
                //base
                color("black")  translate([ 0,0, -lighting_base_h ]) hull(){
                    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ push_joint_xy, push_joint_xy, 3 ], center=true);  }; 
                     minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ push_joint_xy*2, push_joint_xy*2, 3 ], center=true);  }; 
                    }
                    }



//translate([ 0, 0, -50 ]) COP_focus_lens_tubeV3();
 module COP_focus_lens_tubeV3(){//focus control and lens holder all together
    difference() {

        color("green") english_thread (diameter=((tube_r+thread_thick)*2)/25.4, threads_per_inch=32, length=(ext_bot_len+extra_doublet*2+int_focal_corr_h-corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
        
        translate([ 0,0,-corr]) union(){ 
         translate([ 0.00, 0.00, -corr])   color ("lightblue") english_thread (diameter=(M27_r*2)/25.4, threads_per_inch=32, length=(lens_holder_pos+extra_doublet+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
    translate([ 0.00, 0.00, corr ]) color ("red") cylinder(r= foc_adj_r+3*corr, h= ext_bot_len+extra_doublet*2+int_focal_corr_h-thread_h_cam_attach+2*corr);
    translate([ 0.00, 0.00, ext_bot_len+extra_doublet*2+int_focal_corr_h-thread_h_cam_attach+2*corr ]) color ("cyan") english_thread (diameter= (int_focal_corr_r*2)/25.4, threads_per_inch=32, length=(thread_h_cam_attach+2*corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
            echo(str ("total h is ",ext_bot_len+extra_doublet*2+int_focal_corr_h-corr)); 
            
            //notches to stop the whole thing from rotating freely
    reflect(1,0,0) color("blue") translate([tube_r+thread_thick/2+wall/2, 0, (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([ notch_x+corr*2, notch_y+corr*2, ext_bot_len+extra_doublet*2+int_focal_corr_h+corr ], center=true);       
   color("red") translate([0, tube_r+thread_thick/2+wall/2, (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([ notch_y+corr*2, notch_x+corr*2, ext_bot_len+extra_doublet*2+int_focal_corr_h+corr ], center=true);       
  color("red") translate([0, -(tube_r+thread_thick/2+wall/2), (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([ notch_y+corr*2, notch_x+corr*2, ext_bot_len+extra_doublet*2+int_focal_corr_h+corr ], center=true);       
    }}}




    
    
    
    //ruedita_roda();
  module ruedita_roda(){
 //ruedita para girar
      difference(){
      union(){
 translate([0, 0,  rodamiento_h])  
     color("cyan")  difference(){
         minkowski(){
             sphere(2);
             cylinder(r=ruedita_r, h=notch_ring_h/4,$fn=100);
         }
             for (i=[0:360/12:360]) {//number of finger holds
      rotate(i)
      translate([ruedita_r,0,-0.1])
     translate([ 0.00, dedos_d, -2 ]) color("yellow") cylinder(d=dedos_d, h=notch_ring_h);
             }}
          
 translate([0, 0, 0]) color("blue") cylinder(r=rodamiento_i_r, h=rodamiento_h, $fn=100);//aca - corr*3 a un externo  porque rodam es fijo. Luego lo saque porque bailaba
 }
 translate([0, 0, thread_thick-corr*20]) color("green") english_thread (diameter=((tube_r+thread_thick)*2+corr*4)/25.4, threads_per_inch=32, length=(htt_h*1.5)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//corr*3 is the best fit in diam (total eh) corr*4 en Sep2020 para evitar saltos or corr*5 para Ender
 } 
      }   

//ring_M12();     
 module ring_M12(){
     difference(){
       translate([ 0, 0,ring_M12_h/2])  cylinder(r=tube_r,h=ring_M12_h, center=true,$fn=100);
        translate([ 0.00, 0.00, wall])   color ("pink") english_thread (diameter=(M27_r*2)/25.4, threads_per_inch=32, length=(lens_holder_pos+extra_doublet+corr+insert_h)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
         cylinder(r=M12_r+corr,h=ring_M12_h, center=true,$fn=100);
         }
     }     

//ext_M12tube_thrlong();
module ext_M12tube_thrlong(){//esta parte se enrosca al interior del modulo de arriba para agarrar el doublet en su lugar y agarra el M12 invertido
difference() {
    union(){ 
        color("pink") english_thread (diameter=((M27_r*2)-(3*corr))/25.4, threads_per_inch=32, length=(M12ext_h+insert_h/4)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
       translate([0,0,(M12ext_h*3/4)/2+(M12ext_h*1/4)])cylinder(r=tube_r,h=M12ext_h*3/4, center=true,$fn=100);
        
         //tope de espacio de  M12 invertido
   ;}
     //rings for reflections and tope de M12 ring
  translate([0,0,M12_ring_h+(M12ext_h+insert_h)/2]) color("green") cylinder(r1=M12_r,r2=M12_ring_r,h=M12ext_h+insert_h, center=true,$fn=100);
   //internal space to trap reflections
    translate([0,0,internal_space_h/2 +M12_ring_h+wall*2]) color("red") cylinder(r=M12_r*1.5,h=internal_space_h, center=true,$fn=100);
//   english_thread (diameter=(M12_r*2)/25.4, threads_per_inch=18, length=M12ext_h +(insert_h-extra_doublet)/25.4,internal=true, n_starts=1, thread_size=2, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal rough anti-reflective surface 
        //espacio de  M12 invertido
    translate([0,0,M12_ring_h/2])cylinder(r=M12_ring_r+corr/2,h=M12_ring_h, center=true,$fn=100);    
        
    }}

 
  //- to make long the thread inside the cone I will make a one-piece thread dividing COP_focus_lens_tubeV3() as thrlong y el ext_tube tambien
 
// ext_tube_thrlong();
module ext_tube_thrlong(){//esta parte se enrosca al interior del modulo de arriba para agarrar el doublet en su lugar
difference() {
    union(){ 
        translate([0,0,tot_len_bottom/4]) color("pink") english_thread (diameter=((M27_r*2)-(3*corr))/25.4, threads_per_inch=32, length=(insert_h-extra_doublet)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
     cylinder(r=tube_r,h=tot_len_bottom/2, center=true,$fn=100);}
    union() { // this union is the complete internal thread, RMS for objective and anti-reflective internal surface
    translate ([0.00,0.00,-(tot_len_bottom/4)]) color("red") english_thread (diameter=((RMS_r*2)+corr*3)/25.4, threads_per_inch=36, length=(RMS_mount_h)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal RMS thread for DIN (and JIS) objectives 20.1mm / 0.7965" dia. 36 TPI, 55° Whitworth
    translate ([ 0.00, 0.00,-(tot_len_bottom/4) + RMS_mount_h])  color("green") english_thread (diameter=(int_r*2)/25.4, threads_per_inch=32, length=(tot_len_bottom *1.1)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal rough anti-reflective surface 
    }}}

    

// translate([0, 0, htt_h]) ruedita();
  
      
  module ruedita(){
   difference(){
       color("grey")  cylinder(r=tube_r+thread_thick*8, h=htt_h/2,$fn=100);
       translate([0, 0, -corr]) color("grey")  cylinder(r=tube_r+thread_thick*6+corr*3, h=htt_h*2,$fn=100);}
 
 //notch ring for wheel
 difference(){ 
     color("red")  translate([0, 0, htt_h/2]) cylinder(r=tube_r+thread_thick*8, h=notch_x,$fn=100);
 translate([0, 0, -corr]) color("yellow") cylinder(r=notch_ring_r+corr*3, h=htt_h*2,$fn=100);}  
  difference(){ 
     color("green")  translate([0, 0, 0]) cylinder(r=tube_r+thread_thick*4+thread_thick*3, h=notch_x,$fn=100);
 translate([0, 0, -corr]) color("green") cylinder(r=tube_r+thread_thick*2.5+corr*6, h=htt_h*2,$fn=100);}  
 
 //ruedita para girar
 translate([0, 0,  htt_h/2-thread_thick*4]) difference(){ 
     color("cyan")  translate([0, 0, 0]) cylinder(r=tube_r+thread_thick*6, h=notch_ring_h,$fn=100);
// translate([0, 0, -corr]) color("blue") cylinder(r=tube_r+thread_thick+corr*3, h=htt_h*2,$fn=100);
      difference(){ 
          //mueca de ruedita
     color("blue")  translate([0, 0, thread_thick*4]) cylinder(r=tube_r+thread_thick*8+corr, h=notch_x+corr*2,$fn=100);//version4corr tiene corr*4 en lugar de 2 como primer stl
 translate([0, 0, -corr]) color("blue") cylinder(r=notch_ring_r, h=htt_h*2,$fn=100);}  
 // internal thread
 
 translate([0, 0, thread_thick-corr*20]) color("green") english_thread (diameter=((tube_r+thread_thick+corr*3)*2)/25.4, threads_per_inch=32, length=(htt_h*1.1)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
 
 } 
      }      
      
      
module lens_holder_ring(){//internal lens retainer ring
tot_len=2.5;
difference() {
    color("red") english_thread (diameter=(((M27_r*2)-(corr*3))/25.4), threads_per_inch=32, length=tot_len/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal RMS thread for DIN objectives
    translate([ 0.00, 0.00, -corr ])cylinder(r=int_r,h=tot_len+corr*2);
    cube([ 1, 100, 2 ], center=true);
}}



 module picam2_push_fit_FF( beam_length=15){
    // This module is designed to be subtracted from the bottom of a shape.
    // The z=0 plane should be the print bed.
    // It includes cut-outs for the components on the PCB and also a push-fit hole
    // for the camera module.  This uses flexible "fingers" to grip the camera firmly
    // but gently.  Just push to insert, and wiggle to remove.  You may find popping 
    // off the brown ribbon cable and removing the PCB first helps when extracting
    // the camera module again.
    camera = [8.5,8.5,2.8]; //size of camera box (NB it's now propped up on foam)
	cw = camera[0]+1; //side length of camera box at bottom (slightly larger)
	finger_w = 1.5; //width of flexure "fingers"
	flex_l = 1; //width of flexible part
    hole_r = camera[0]/2-0.4;
	union(){
       
		//cut-out for camera
        /*hull(){
            translate([0,0,-d]) cube([cw+0.5,cw+0.5,d],center=true); //hole for camera
            translate([0,0,1]) cube([cw-0.5,cw-0.5,d],center=true); //hole for camera
        }
        */
        rotate(180/16) cylinder(r=hole_r,h=beam_length,center=true,$fn=100); //hole for light
        
        //looser cut-out for camera, with gripping "fingers" on 3 sides
        difference(){
            //cut-out big enough to include gripping fingers
           /* intersection(){
                hull(){
                    translate([0,-(finger_w+flex_l)/2,0.5+d])
                        cube([cw+2*finger_w+2*flex_l, cw+finger_w+flex_l, 2*d],center=true);
                    translate([0,0,0.5+3*(finger_w+flex_l)]) cube([cw, cw, d],center=true);
                }
                //fill in the corners of the void first, to give an endstop for the camera
                
                //build up the roof gradually so we get a nice hole
                rotate(90) translate([0,0,camera[2]+1.0]) 
                    hole_from_bottom(r=hole_r,h=beam_length - camera[2]-1.5);
            }
            */
                
            //gripping "fingers" (NB we subtract these from the cut-out)
           /* for(a=[90:90:270]) rotate(a) hull(){
                translate([-cw/2+0.5,cw/2,0]) cube([cw-1,finger_w,d]);
                translate([-cw/2+1,camera[0]/2-0.1,camera[2]]) cube([cw-2,finger_w,d]);
            }
            */
            //there's no finger on the top, so add a dimple on the fourth side
            /*hull(){
                translate([-cw/2+1,cw/2,4.3/2]) cube([cw-2,d,camera[2]-1.5]);
                translate([-cw/2+2,camera[1]/2,camera[2]-0.5]) cube([cw-4,d,0.5]);
                translate([-21/2,cw/2,camera[2]-1.5]) cube([21,d,camera[2]-1.5]);
                translate([-21/2,camera[1]/2,camera[2]-0.5]) cube([21,d,0.5]);
            }
            */
		}
        
		//ribbon cable at top of camera
        sequential_hull(){
            translate([0,-5,0]) cube([cw-1,d,5],center=true);
            translate([0,cw/2+1,0]) cube([cw-1,d,5],center=true);
            translate([0,9.4-(4.4/1)/2,0]) cube([cw-1,1,5],center=true);
        }
        //flex connector
        translate([-1.25,9.4,0]) cube([cw-1+2.5, 4.4+1, 5],center=true);
        
		//screw holes for safety (M2 "threaded")
		reflect([1,0,0]) translate([21/2,0,0]){
            cylinder(r1=2.5, r2=1, h=2, center=true, $fn=100);
            cylinder(r=1, h=7, $fn=100);
        }
	}
}


module camera_mount_FF(){
    // A mount for the pi camera v2
    // This should finish at z=0+d, with a surface that can be
    // hull-ed onto the lens assembly.
    h = 24;
    w = 25;
    rotate(45) difference(){
        translate([0,2.4,0]) sequential_hull(){
            translate([0,0,bottom]) cube([w,h,d],center=true);
            translate([0,0,bottom+1.5]) cube([w,h,d],center=true);
            translate([0,0,0]) cube([w-(-1.5-bottom)*2,h,d],center=true);
        }
        translate([0,0,bottom]) picam2_push_fit_FF();
    }
}




/* ------------------------------------------------------------------------
* ------------------------------------------------------------------------
* ------------------------------------------------------------------------
* all the modules below are to test your printing resolution
* ------------------------------------------------------------------------
* ------------------------------------------------------------------------
* ------------------------------------------------------------------------ */
    
module test_print_femaleC(){
//render() {
tot_len_bottom=10;//total len of a single piece bottom
difference() {
    cylinder(r=tube_r,h=tot_len_bottom,$fn=100);
    translate ([0.00,0.00,-(tot_len_bottom/3)]) color("red") 
    english_thread (diameter=(((c_mount_r*2)+(corr*3))/25.4), threads_per_inch=32, length=(tot_len_bottom *1.1)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal rough antireflective surface 
 }}
    
module test_print_femaleM27(){//Bottom part merges DIN_C_mount, 40_ext and the bottom half of Lens_mount
//render() {
tot_len_bottom=10;//total len of a single piece bottom
difference() {
    cylinder(r=tube_r,h=tot_len_bottom,$fn=100);
    translate ([0.00,0.00,-(tot_len_bottom/3)]) color("red") 
    english_thread (diameter=(((M27_r*2)+(corr*0))/25.4), threads_per_inch=32, length=(tot_len_bottom *1.1)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal rough antireflective surface 
 }}
 
 module test_femaleRMS(){//test printing resolution and fitting with RMS objectives, etc
tot_len_bottom=10;
echo(str ("int thread is ",((RMS_r*2)+corr*3))); 
difference() {
         cylinder(r=tube_r,h=tot_len_bottom/2);
    color("red") english_thread (diameter=((RMS_r*2)+corr*3)/25.4, threads_per_inch=36, length=tot_len_bottom/2/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal RMS thread for DIN (and JIS) objectives
}
}
//
//module test_print_thread(){//test printing resolution and fitting between male and female threads 
//    tot_len_bottom=10;
//        translate([0,0,tot_len_bottom/2]) color("lightblue") english_thread (diameter=((c_mount_r*2)+2*corr)/25.4, threads_per_inch=32, length=(lens_holder_male_len)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//this size (printed in a Zortrax M200 in high quality Z-ABS) fits perfectly in female Edmunds optics metallic parts 
//         cylinder(r=tube_r,h=tot_len_bottom/2,$fn=100);
//     }
     
module test_maleM27(){//test printing resolution and fitting between male and female threads 
    tot_len_bottom=10;
        translate([0,0,tot_len_bottom/2]) color("lightblue") english_thread (diameter=((M27_r*2)-3*corr)/25.4, threads_per_inch=32, length=(lens_holder_male_len*1.5)/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);//this size (printed in a Zortrax M200 in high quality Z-ABS) fits perfectly in female Edmunds optics metallic parts 
         cylinder(r=tube_r,h=tot_len_bottom/2,$fn=100);
     }
    


module focus_adjust_tube(){
    difference() {
    union(){ 
        translate([0,0,int_focal_corr_h]) color("lightblue") english_thread (diameter=(foc_adj_r*2)/25.4, threads_per_inch=32, length=16/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
        translate([ 0,0, int_focal_corr_h/2 ]) cube([tube_r*2,tube_r*2, int_focal_corr_h],center=true,$fn=100);
//     cylinder(r=tube_r,h=int_focal_corr_h,$fn=100);
        }
    translate ([0.00,0.00,-(corr)]) color("red") english_thread (diameter=(int_focal_corr_r*2)/25.4, threads_per_inch=32, length=((tot_len_focus_adjust_tube)+(2*corr))/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
     }    
    }

  
// COP_lens_holderV2();     
module COP_lens_holderV2(){//for doublet
    difference() {
        color("green") translate([ 0, 0, (ext_bot_len+extra_doublet*2)/2 ]) cube([tube_r*2,tube_r*2, ext_bot_len+extra_doublet*2-corr],center=true,$fn=100);
        translate([ 0,0,-corr]) union(){ 
         translate([ 0.00, 0.00, -corr])   color ("lightblue") english_thread (diameter=(M27_r*2)/25.4, threads_per_inch=32, length=(lens_holder_pos+extra_doublet+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
    translate([ 0.00, 0.00, corr ]) color ("red") english_thread (diameter=((foc_adj_r*2)+(3*corr))/25.4, threads_per_inch=32, length=(ext_bot_len+extra_doublet*2+2*corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
    translate([0,tube_r, ext_bot_len+extra_doublet- M3_brass_push_h]) rotate([90,90,0]) cylinder(r=M3_brass_push_h/2,h=(M3_brass_push_h)*2,$fn=100);
    }}}
// COP_focus_lens_holder(); 
 module COP_focus_lens_holder(){
    difference() {
        color("green") translate([ 0, 0, (ext_bot_len+extra_doublet*2+int_focal_corr_h)/2 ]) cube([tube_r*2,tube_r*2, ext_bot_len+extra_doublet*2+int_focal_corr_h-corr],center=true,$fn=100);
        translate([ 0,0,-corr]) union(){ 
         translate([ 0.00, 0.00, -corr])   color ("lightblue") english_thread (diameter=(M27_r*2)/25.4, threads_per_inch=32, length=(lens_holder_pos+extra_doublet+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
    translate([ 0.00, 0.00, corr ]) color ("red") cylinder(r= foc_adj_r+3*corr, h= ext_bot_len+extra_doublet*2+int_focal_corr_h-thread_h_cam_attach+2*corr);
    translate([ 0.00, 0.00, ext_bot_len+extra_doublet*2+int_focal_corr_h-thread_h_cam_attach+2*corr ]) color ("cyan") english_thread (diameter= (int_focal_corr_r*2)/25.4, threads_per_inch=32, length=(thread_h_cam_attach+2*corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
            echo(str ("total h is ",ext_bot_len+extra_doublet*2+int_focal_corr_h-corr)); 
    }}}
    
    /*-----------------------old modules--------------------------------*/
//cone_base(); //cono de abajo que presiona con notches sobre el tubo optico 
//RPIcam_cover();
//focus_adjust_tube();
//lens_holder(); // or 
//lens_holder_V2();
//COP_lens_holderV2();   //this one can accommodate either a simple planoconvex comar lens or the bigger achromatic doublet
//cam_adapter();
//camera_mount_FF();
    
 
// cone_base_slide_V2();      
module cone_base_slide_V2(){
  translate([ 0.00, 0.00,push_joint_h]){
   difference(){     
       //cone
   union(){
       color("pink")  hull(){ translate([ 0.00, 0.00, cone_slide_h ]) 
            cylinder(r=tube_r+thread_thick*4, h=1);
            minkowski(){ 
                    cylinder(r=wall,h=1); 
                    cube([ push_joint_xy_V2, push_joint_xy_V2, 1.00 ], center=true);  }};
      
       //slide holder  
 difference(){
     union(){ translate([ 0.00, 0.00, 1 ]) color("red") 
      minkowski(){ sphere(r=1,h=1+corr); cube([ push_joint_xy_V2, slide_y*1.5, 1 ], center=true); }  
       sequential_hull(){
       translate([ 0.00, 0.00, 1 ]) minkowski(){ sphere(r=1,h=1+corr); cube([ push_joint_xy_V2, slide_y*1.5, 1 ], center=true); }  
      translate([ 0.00, 0.00, -wall ])  minkowski(){ sphere(r=2,h=1+corr); cube([ push_joint_xy*0.7, slide_y*0.85, 1 ], center=true); } 
     
      translate([ 0.00, 0.00, -push_joint_h*0.8 ]) 
                     cube([ push_joint_xy*0.4, push_joint_xy, 1 ], center=true);  
      }
       translate([ slide_x/1.8,  push_joint_xy, -1 ])  cylinder(r=2*1.2,h=6, center=true); 
        translate([ slide_x/1.8,  -push_joint_xy, -1])  cylinder(r=2*1.2,h=6, center=true); 
                    }
                    
       color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h*2, , center=true); 
                    cube([ push_joint_xy_V2-wall, push_joint_xy_V2-wall,  1 ], center=true);  }
                    }}
                    
   color("yellow")   hull(){ translate([ 0.00, 0.00, cone_slide_h ]) 
       cylinder(r=tube_r+thread_thick+corr*6, h=1);
            minkowski(){ 
                    cylinder(r=wall,h=1+corr); 
                    cube([ push_joint_xy_V2-wall*3, push_joint_xy_V2*0.7-wall*3,  1.00 ], center=true);  }};
          
       //slide holder operation space        
       translate([ 0.00, 0.00, slide_holder_z/2+1.5 ]) minkowski(){ 
                    sphere(); translate([ 0.00, 0.00, 2.00 ])cube([ slide_x*1.85, slide_y+wall, slide_holder_z ], center=true); }      
                    
       //slide view space      
       translate([ 0.00, 0.00, slide_holder_z/2 ]) rotate([ 0,0,90])   minkowski(){ 
                    sphere(r=wall,h=1+corr); 
           cube([ slide_x*0.4, slide_y, slide_holder_z/2 ], center=true);  
       } 
       //holes in holder to screw clips in
       translate([ slide_x/1.8,  push_joint_xy_V2, 0 ])  cylinder(r=3/2*1.2,h=100000, center=true); 
        translate([ slide_x/1.8,  -push_joint_xy_V2, 0 ])  cylinder(r=3/2*1.2,h=100000, center=true); 
                    }
translate([ 0.00, 0.00,cone_slide_h]) 
  union(){
      difference(){
        color("grey")  sequential_hull(){ 
            translate([ 0.00, 0.00, htt_h ])   cylinder(r=rodamiento_e_r+thread_thick,h=1); 
            translate([ 0.00, 0.00, htt_h*0.7 ])   cylinder(r=rodamiento_e_r*0.8,h=1); 
            cylinder(r=tube_r+thread_thick*4, h=1);   };
                    
//          cylinder(r=tube_r+thread_thick*4, h=htt_h,$fn=100);
                translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*4,$fn=100);}
 
    //notches
 reflect() color("blue") translate([(tube_r+thread_thick/2+wall/2)+corr, 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);       
            color("red") translate([0, tube_r+thread_thick/2+wall/2+corr, htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);       
            color("cyan") translate([0, -(tube_r+thread_thick/2+wall/2+corr), htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);     
 
translate([ 0.00, 0.00, htt_h]) difference(){
    translate([ 0, 0, 0 ]) color("blue")  cylinder(r=rodamiento_e_r+thread_thick, h=rodamiento_h,$fn=100);
    translate([0, 0, thread_thick]) color("pink")  cylinder(r=rodamiento_e_r+corr, h=rodamiento_h,$fn=100);
    translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);
    }
}
        }   
//push_joint between cone and illumination base
  translate([ 0.00, 0.00,push_joint_h/2]){
      difference(){
     color("green") minkowski(){ 
                    cylinder(r=wall,h=push_joint_h, center=true); 
                     cube([ push_joint_xy_V2, push_joint_xy_V2, 1 ], center=true);  };
    color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h+corr , center=true); 
                    cube([ push_joint_xy_V2-wall, push_joint_xy_V2-wall,  1 ], center=true);  }
                    }}
        }




// cone_base_slide();      
module cone_base_slide(){
  translate([ 0.00, 0.00,push_joint_h]){
   difference(){     
       //cone
   union(){
       color("pink")  hull(){ translate([ 0.00, 0.00, cone_slide_h ]) 
            cylinder(r=tube_r+thread_thick*4, h=1);
            minkowski(){ 
                    cylinder(r=wall,h=1); 
                    cube([ push_joint_xy, push_joint_xy, 1.00 ], center=true);  }};
      
       //slide holder  
 difference(){
     union(){ translate([ 0.00, 0.00, 1 ]) color("red") 
      minkowski(){ sphere(r=1); cube([ push_joint_xy, slide_y*1.2, 1 ], center=true); }  
       sequential_hull(){
       translate([ 0.00, 0.00, 1 ]) minkowski(){ sphere(r=1); cube([ push_joint_xy, slide_y*1.2, 1 ], center=true); }  
      translate([ 0.00, 0.00, -wall ])  minkowski(){ sphere(r=2); cube([ push_joint_xy*0.7, slide_y*0.85, 1 ], center=true); } 
     
      translate([ 0.00, 0.00, -push_joint_h*0.8 ]) 
                     cube([ push_joint_xy*0.4, push_joint_xy, 1 ], center=true);  
      }
       translate([ slide_x/1.8,  push_joint_xy, -1 ])  cylinder(r=2*1.2,h=6, center=true); 
        translate([ slide_x/1.8,  -push_joint_xy, -1])  cylinder(r=2*1.2,h=6, center=true); 
                    }
                    
       color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h*2, , center=true); 
                    cube([ push_joint_xy-wall, push_joint_xy-wall,  1 ], center=true);  }
                    }}
                    
   color("yellow")   hull(){ translate([ 0.00, 0.00, cone_slide_h ]) 
       cylinder(r=tube_r+thread_thick+corr*6, h=1);
            minkowski(){ 
                    cylinder(r=wall,h=1+corr); 
                    cube([ push_joint_xy-wall*3, push_joint_xy-wall*3,  1.00 ], center=true);  }};
          
       //slide holder operation space        
       translate([ 0.00, 0.00, slide_holder_z/2+1.5 ]) minkowski(){ 
                    sphere(); translate([ 0.00, 0.00, 2.00 ])cube([ slide_x*1.35, slide_y+wall, slide_holder_z ], center=true); }      
                    
       //slide view space      
       translate([ 0.00, 0.00, slide_holder_z/2 ]) rotate([ 0,0,90])   minkowski(){ 
                    sphere(r=wall); 
           cube([ slide_x*0.4, slide_y, slide_holder_z/2 ], center=true);  
       } 
       //holes in holder to screw clips in
       translate([ slide_x/1.8,  push_joint_xy, 0 ])  cylinder(r=3/2*1.2,h=100000, center=true); 
        translate([ slide_x/1.8,  -push_joint_xy, 0 ])  cylinder(r=3/2*1.2,h=100000, center=true); 
                    }
translate([ 0.00, 0.00,cone_slide_h]) 
  union(){
      difference(){
        color("grey")  sequential_hull(){ 
            translate([ 0.00, 0.00, htt_h ])   cylinder(r=rodamiento_e_r+thread_thick,h=1); 
            translate([ 0.00, 0.00, htt_h*0.7 ])   cylinder(r=rodamiento_e_r*0.8,h=1); 
            cylinder(r=tube_r+thread_thick*4, h=1);   };
                    
//          cylinder(r=tube_r+thread_thick*4, h=htt_h,$fn=100);
                translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*4,$fn=100);}
 
    //notches
 reflect() color("blue") translate([(tube_r+thread_thick/2+wall/2)+corr, 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);       
            color("red") translate([0, tube_r+thread_thick/2+wall/2+corr, htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);       
            color("cyan") translate([0, -(tube_r+thread_thick/2+wall/2+corr), htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);     
 
translate([ 0.00, 0.00, htt_h]) difference(){
    translate([ 0, 0, 0 ]) color("blue")  cylinder(r=rodamiento_e_r+thread_thick, h=rodamiento_h,$fn=100);
    translate([0, 0, thread_thick]) color("pink")  cylinder(r=rodamiento_e_r+corr, h=rodamiento_h,$fn=100);
    translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);
    }
}
        }   
//push_joint between cone and illumination base
  translate([ 0.00, 0.00,push_joint_h/2]){
      difference(){
     color("green") minkowski(){ 
                    cylinder(r=wall,h=push_joint_h, center=true); 
                     cube([ push_joint_xy, push_joint_xy, 1 ], center=true);  };
    color("red")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h, , center=true); 
                    cube([ push_joint_xy-wall, push_joint_xy-wall,  1 ], center=true);  }
                    }}
        }


 
 
      
//translate([ 0, 0, -50 ]) cone_base();
 module cone_base(){
    difference(){//cone
      color("pink")  cylinder(r2=tube_r+thread_thick*4, r1=rodamiento_e_r*1.5+thread_thick, h=cono_h,$fn=100);
      color("yellow")  cylinder(r2=tube_r+thread_thick+corr*6, r1=rodamiento_e_r*1.5, h=cono_h,$fn=100);
  }
             //notches to stop the whole thing from rotating freely

translate([ 0.00, 0.00,cono_h]) 
  union(){
      difference(){
    color("grey")  cylinder(r=tube_r+thread_thick*4, h=htt_h,$fn=100);
 translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);}
     reflect() color("blue") translate([(tube_r+thread_thick/2)+corr*3, 0, htt_h/2 ]) cube([ notch_x, notch_y, htt_h ], center=true);       
color("red") translate([0, tube_r+thread_thick/2+corr*3, htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);       
  color("cyan") translate([0, -(tube_r+thread_thick/2)-corr*3, htt_h/2 ]) cube([ notch_y, notch_x, htt_h ], center=true);     

translate([ 0.00, 0.00, htt_h]) difference(){
   translate([ 0, 0, 0 ]) color("blue")  cylinder(r=rodamiento_e_r+thread_thick, h=rodamiento_h,$fn=100);
translate([0, 0, thread_thick]) color("pink")  cylinder(r=rodamiento_e_r+corr, h=rodamiento_h,$fn=100);
    translate([0, 0, -corr]) color("yellow")  cylinder(r=tube_r+thread_thick+corr*6, h=htt_h*2,$fn=100);
    }
}
        }
        

//ext_tube();
module ext_tube(){//Bottom part replaces edmund optics parts: DIN_C_mount, 40_ext and the bottom half of Lens_mount
//render() {
difference() {
    union(){ 
        translate([0,0,tot_len_bottom/2]) color("lightblue") english_thread (diameter=((M27_r*2)-(3*corr))/25.4, threads_per_inch=32, length=3.5/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
     cylinder(r=tube_r,h=tot_len_bottom, center=true,$fn=100);}
    union() { // this union is the complete internal thread, RMS for objective and anti-reflective internal surface
    translate ([0.00,0.00,-(tot_len_bottom/2)]) color("red") english_thread (diameter=((RMS_r*2)+corr*3)/25.4, threads_per_inch=36, length=(RMS_mount_h)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal RMS thread for DIN (and JIS) objectives 20.1mm / 0.7965" dia. 36 TPI, 55° Whitworth
    translate ([ 0.00, 0.00,-(tot_len_bottom/2) + RMS_mount_h])  color("green") english_thread (diameter=(int_r*2)/25.4, threads_per_inch=32, length=(tot_len_bottom *1.1)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal rough anti-reflective surface 
    }}}
 
//    ext_fluo_top();
module ext_fluo_top(){
    difference() {
    union(){ 
     translate([0,0,fluo_top_h/2]) color("lightblue") english_thread (diameter=((M27_r*2)-(3*corr))/25.4, threads_per_inch=32, length=3.5/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
     cylinder(r=tube_r,h=fluo_top_h, center=true,$fn=100);}
    union() { // this union is the complete internal thread: anti-reflective internal surface + M27 for trapping the polarizer (or fluo filter) with the fluo_bottom
    translate ([0.00,0.00,-(fluo_top_h/2)]) color("red") english_thread (diameter=(int_r*2)/25.4, threads_per_inch=32, length=(fluo_top_h*2)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal M27 thread
    translate([ 0.00, 0.00, -((fluo_top_h/2)+corr)])   color ("green") english_thread (diameter=(M27_r*2)/25.4, threads_per_inch=32, length=(pol_ring_h+corr)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
    }}}
    
//    ext_fluo_bottom();
module ext_fluo_bottom(){//Bottom part for fluorescent or polarizing filters
difference() {
    union(){ 
        translate([0,0,fluo_bottom_h/2]) color("lightblue") english_thread (diameter=((M27_r*2)-(3*corr))/25.4, threads_per_inch=32, length=3.5/25.4,internal=false, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);
     cylinder(r=tube_r,h=fluo_bottom_h, center=true,$fn=100);}
    union() { // this union is the complete internal thread, RMS for objective and anti-reflective internal surface
    translate ([0.00,0.00,-(fluo_bottom_h/2)]) color("red") english_thread (diameter=((RMS_r*2)+corr*3)/25.4, threads_per_inch=36, length=(RMS_mount_h)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal RMS thread for DIN (and JIS) objectives 20.1mm / 0.7965" dia. 36 TPI, 55° Whitworth
    translate ([ 0.00, 0.00,-(fluo_bottom_h/2) + RMS_mount_h])  color("green") english_thread (diameter=(int_r*2)/25.4, threads_per_inch=32, length=(fluo_bottom_h *1.1)/25.4,internal=true, n_starts=1, thread_size=-1, groove=true,square=false, rectangle=0, angle=30, taper=0, leadin=1);// this creates an internal rough anti-reflective surface 
    }}}
    

