Este es un repositorio que contiene archivos de diseño (editables en openSCAD) y STLs listos para imprimir diferentes microscopios de hardware abierto. Estos dispositivos han sido diseñados para diferentes proyectos en el marco del proyecto Fondecyt Regular 1211218, en colaboración con Nano Castro de la comunidad [reGOSH](https://regosh.libres.cc/en/home-en/)..  

Todos los diseños se basan en el uso de una "ajuste rotatorio" superior que permite hacer foco de manera precisa sin la necesidad de incorporar otros elementos mecánicos más sofisticados (e.g. ejes, motores paso a paso); de aquí el nombre de _saleroscopio_ ya que parece un [salero giratorio](https://www.google.com/search?q=salero%20giratorio&tbm=isch&tbs=rimg:CWgmaYSEkCMlYZyTLyNClFEh&hl=es-419&sa=X&ved=0CAIQrnZqFwoTCIDM1faT0uwCFQAAAAAdAAAAABAO&biw=1919&bih=946). La óptica de los dispositivos esta diseñada a partir de otros diseños ya existentes en la comunidad de hardware científico abierto, tales como [openflexure](https://www.youtube.com/watch?v=0F1pBmWuU3M)), o réplicas de impresión 3D de piezas comerciales como las desarrolladas en el proyecto [Comunicaciones Especulativas](http://interspecifics.cc/comunicacionesespeculativas/). De esta manera, los diseños se nutren (_copy-transform-remix_) de codigo generado en otros proyectos de hardware científico abierto (más avanzados) y los adapta a las necesidades de nuestars actividades.

Proyectos en este repo:

**[SaleroFluor](https://gitlab.com/FernanFederici/saleroscopio/-/tree/master/Salero_fluor?ref_type=heads)**: microscopio de fluorescencia y optogenètica, desarrollado en el contexto del proyecto Fondecyt Regular 1211218.

![3D printed epi](Images/PXL_20231117_142002501.jpg){width=40%}
![3D printed epi](Images/PXL_20231121_183015122.jpg){width=40%} 
![3D printed epi](Images/PXL_20231121_183033190.jpg){width=40%} 
![STL files](Images/STLs.jpg){width=60%} 

**[birrescopio](https://gitlab.com/FernanFederici/saleroscopio/-/tree/master/other_versions_inprogress/birrescopio)**: microscopio de birrefringencia, para ver cristales microscopicos. (Le falta orden a esa carpeta, pero la vida es corta. Work in progess / en construccion...)

**[scannerscopio](https://gitlab.com/FernanFederici/saleroscopio/-/tree/master/other_versions_inprogress/scannerscopio)**: desarrollado con Seba Rodriguez [laboratorio Biofab UC](https://www.instagram.com/biofab.uc/?hl=es-la). El objetivo principal de este dispositivo es permitir la obtención de imagenes de manera rápida y simple. Por ejemplo, tener un dispositivo que permita a lxs visitantes de una exhibición manipular el dispositivo (e.g. scanear el biomaterial, hacer foco y tomar fotos) de manera intuitiva, ojalá sin la necesidad de una persona guia. (Le falta orden a esa carpeta, pero la vida es corta. Work in progess / en construccion...)

**[microscopio de terreno](https://gitlab.com/FernanFederici/saleroscopio/-/tree/master/other_versions_inprogress/microscopio_terreno)**: en vias de desarrollo para microfotografia de terreno. (work in progess / en construccion...)



